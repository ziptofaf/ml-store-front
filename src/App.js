import React, { Component } from 'react';
import Home from './components/Home';
import Header from './components/shared/Header';
import Footer from './components/shared/Footer';
import Products from './components/Products';
import Basket from './components/Basket';
import OrderAddress from './components/orders/Address';
import OrderSummary from './components/orders/Summary';
import {Questions, Returns, Terms, Privacy} from './components/faqs';
import {Delivery, PaymentMethods, Contact} from "./components/paymentsContacts";
import Register from './components/Registration';
import Login from './components/Login';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import {OrderShow} from "./components/orders/Show";
import OrdersList from "./components/orders/List";


class App extends Component {
  render() {
    return (
        <Router>
          <div className="App">
            <Header/>
            <Route exact path="/" component={Home} />
            <Route exact path="/terms" component={Terms} />
            <Route exact path="/questions" component={Questions} />
            <Route exact path="/returns" component={Returns} />
            <Route exact path="/privacy" component={Privacy} />
            <Route exact path='/basket' component={Basket} />
            <Route exact path='/new-order' component={OrderAddress} />
            <Route exact path='/order-summary' component={OrderSummary}/>
            <Route exact path='/register' component={Register}/>
            <Route exact path='/login' component={Login}/>
            <Route exact path='/delivery' component={Delivery}/>
            <Route exact path='/payment-methods' component={PaymentMethods}/>
            <Route exact path='/contact-us' component={Contact}/>
            <Route exact path='/orders' component={OrdersList}/>
            <Route path='/orders/:id' component={OrderShow} />
            <Route path='/products' component={Products} />
            <Footer/>
          </div>
        </Router>
    );
  }
}

export default App;
