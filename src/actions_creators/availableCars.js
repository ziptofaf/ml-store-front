import {CARS_AVAILABLE_CHANGE} from "../constants/actionTypes";

function doChangeAvailableCars(availableCars, selectedCar=undefined) {
    return {
      type: CARS_AVAILABLE_CHANGE,
      availableCars: availableCars,
      selectedCar: selectedCar
    };
}

export {doChangeAvailableCars};