import {BASKET_ADD, BASKET_EMPTY, BASKET_REMOVE} from "../constants/actionTypes";

function doAddToBasket(product, quantity) {
    return {
        type: BASKET_ADD,
        product: product,
        quantity: quantity
    };
}

function doRemoveFromBasket(product, quantity) {
    return {
        type: BASKET_REMOVE,
        product: product,
        quantity: quantity
    }
}

function doEmptyBasket() {
    return {
        type: BASKET_EMPTY
    }
}

export {doAddToBasket, doRemoveFromBasket, doEmptyBasket};