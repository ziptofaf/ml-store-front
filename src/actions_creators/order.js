import {ORDER_CHANGE} from "../constants/actionTypes";

function doChangeOrderAttribute(attribute) {
    return {
        type: ORDER_CHANGE,
        attribute: attribute
    };
}

export {doChangeOrderAttribute};