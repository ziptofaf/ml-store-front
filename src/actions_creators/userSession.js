import {USER_SESSION_CHANGE} from "../constants/actionTypes";

function doChangeUserSessionToken(token) {
    return {
        type: USER_SESSION_CHANGE,
        user_session: {token: token},
    };
}

export {doChangeUserSessionToken};
