import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Breadcrumbs} from "../shared/Breadcrumbs";
import {doAddToBasket, doRemoveFromBasket} from "../../actions_creators/basket";
import MAIN_API_PATH from "../../constants/urls";
import { Link } from "react-router-dom";
import {itemsCountToWord} from "../../helper_functions/translations/wordCountVariations";
import {sumDeliveriesInBasket, sumProductsInBasket, sumVatInBasket, formatPriceToString, priceWithQuantity} from "../../helper_functions/priceHelpers";
import {doChangeOrderAttribute} from "../../actions_creators/order";
import {paymentOptions, deliveryOptions} from "../../constants/orderOptions";

class Basket extends Component {
    paramsForBreadcrumbs = () => {
        return [{name: 'Koszyk', path: '/basket'}]
    };
    reduceQuantity = (item) => {
        if (item.quantity === 1) {return;}
        this.props.onRemoveFromBasket(item, item.quantity - 1);
    };
    increaseQuantity = (item) => {
        console.log('aaa');
        console.log(item);
        let quantity = 1;
        if (item.quantity>2) {return;}
        this.props.onAddToBasket(item, quantity);
    };
    removeFromBasket = (item) => {
        this.props.onRemoveFromBasket(item, 0);
    };


    changeDelivery = (event) => {
        let value = event.target.value;
        this.props.onOrderAttributeChange({delivery_option: value});
    };

    changePaymentType = (event) => {
        let value = event.target.value;
        this.props.onOrderAttributeChange({payment_attributes: {payment_system: value}});
    };
    render() {
        const {inBasket} = this.props;
        const {payment_attributes, delivery_option} = this.props.orderState;
        const paymentType = payment_attributes['payment_system'];
        const priceWithoutShipping = sumProductsInBasket(inBasket);
        const shippingPrice = sumDeliveriesInBasket(inBasket, delivery_option);
        const vatPrice = sumVatInBasket(inBasket, shippingPrice);
        const totalPrice = sumProductsInBasket(inBasket, shippingPrice);
        return (
        <React.Fragment>
            <Breadcrumbs name='Koszyk' urls={this.paramsForBreadcrumbs()}/>
            <section id="content">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-12">
                            <ul className="cart-items">
                                {inBasket.map((item) => (
                                    <li className="cart-item" key={item.id}>
                                        <div className="product-line-grid">
                                            <div className="row">
                                                <div className="col-lg-2 col-md-2 col-3 product-photo">
                                                    <img className="img-fluid" src={MAIN_API_PATH+item.image_url} alt="zdjęcie produktu"/>
                                                </div>
                                                <div className="col-lg-4 col-md-4 col-9 vertical-center">
                                                    <h4><Link to={'/products/'+item.id}>{item.name}</Link></h4>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-12 vertical-center">
                                                    <div className="row">
                                                        <div className="col-lg-5 col-md-5 col-3 offset-3 offset-sm-0">
                                                            <div className="quantity">
                                                                <span className='clickable' onClick={() => this.reduceQuantity(item)}>-</span>
                                                                <span>{item.quantity}</span>
                                                                <span className='clickable' onClick={() => this.increaseQuantity(item)}>+</span>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-4 vertical-center">
                                                            <span className="price">{formatPriceToString(priceWithQuantity(item))}</span>
                                                        </div>
                                                        <div className="col-lg-3 col-md-3 col-2 vertical-center">
                                                            <div className="trash">
                                                                <i className="far fa-trash-alt" onClick={() => this.removeFromBasket(item)}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            <Link to="/products"><span className="continue"><i className="fas fa-chevron-left"/>Kontynuuj zakupy</span></Link>
                        </div>
                        <div className="col-lg-4 marginMobileTop">
                            <div className="cart-summary">
                                <div className="cart-block">
                                    <div className="cart-summary-line">
                                        <span className="sub-total">{inBasket.length} {itemsCountToWord(inBasket)}</span>
                                        <span className="value">{formatPriceToString(priceWithoutShipping)}</span>
                                    </div>
                                    <div className="cart-summary-line">
                                        <span className="sub-total">Koszty przesyłki</span>
                                        <span className="value">{formatPriceToString(shippingPrice)}</span>
                                    </div>
                                </div>
                                <hr/>
                                <div className="cart-block">
                                    <div className="cart-summary-line">
                                        <span className="sub-total">Całkowita kwota</span>
                                        <span className="value">{formatPriceToString(totalPrice)}</span>
                                    </div>
                                    <div className="cart-summary-line">
                                        <span className="sub-total">Podatek VAT</span>
                                        <span className="value">{formatPriceToString(vatPrice)}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="rest-summary">
                                <h4>Dostawa</h4>
                                {deliveryOptions.map((item) => (
                                < div className="cart-summary-line" key={item.value}>
                                    <label className="radio">
                                    {item.name}
                                    <input type="radio" value={item.value} checked={delivery_option === item.value} name="delivery" onChange={this.changeDelivery}/>
                                    <span className="checkmark"/>
                                    </label>
                                    <span className="value">{item.price} zł</span>
                                    </div>
                                ))}
                                <h4 style={{marginTop: "20px"}}>Płatności</h4>
                                {
                                    paymentOptions.map((item) => (
                                        <div className="cart-summary-line" key={item.value}>
                                            <label className="radio">
                                                {item.name}
                                                <input type="radio" value={item.value} checked={paymentType === item.value} name="payment" onChange={this.changePaymentType}/>
                                                <span className="checkmark"/>
                                            </label>
                                        </div>
                                    ))
                                }

                            </div>
                            <Link to="new-order"><div className="button">Przejdź do zamówienia</div></Link>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAddToBasket: (product, quantity) => dispatch(doAddToBasket(product, quantity)),
        onRemoveFromBasket: (product, quantity) => dispatch(doRemoveFromBasket(product, quantity)),
        onOrderAttributeChange: (attribute) => dispatch(doChangeOrderAttribute(attribute)),
    };
}

function mapStateToProps(state) {
    return {
        inBasket: state.basketState.inBasket,
        orderState: state.orderState
    };
}

const ConnectedBasket = connect(mapStateToProps, mapDispatchToProps)(Basket);

export default ConnectedBasket;