import TowbarFinder from './partials/TowbarFinder';
import React, { Component } from 'react';

class Home extends Component {
    render() {
        return (
            <TowbarFinder/>
        );
    }
}

export default Home;