import React, { Component } from 'react';
import { connect } from 'react-redux';
import { doChangeAvailableCars } from "../../../actions_creators/availableCars";
import fetchFunction from '../../../helper_functions/fetcher';
import {
    Link
} from 'react-router-dom';


class TowbarFinder extends Component {
    constructor(props) {
        super(props);
        this.onAvailableCarsChange = props.onAvailableCarsChange;
        this.state = {
            selectedMake: 0,
            selectedModel: 0,
            selectedBodyType: 0,
            selectedYear: 0
        }
    }

    fetchMakes = () => {
        console.log(this.props.availableCars);
        if (this.props.availableCars.make.length > 0) {return;}
        const relative_path = "product_categories";
        const params = {root_category: 1};
        fetchFunction(relative_path, this.checkIfMakeFetchSuccessful, this.handleFetchError, params);
    };

    checkIfMakeFetchSuccessful = (res) => {
        if (Array.isArray(res)) {
            const availableCars = {make: res, model: [], bodyType: [], year: []};
            this.onAvailableCarsChange(availableCars, undefined);
        }
        else {
            throw new Error('Cant connect to API or invalid category');
        }
    };

    handleFetchError = (e) => {
        console.log(e);
    };

    fetchModels = () => {
        const {selectedMake} = this.state;
        if (parseInt(selectedMake) === 0) {
            const selectedCar = {make: this.props.availableCars.make, model: [], bodyType: [], year: []};
            this.onAvailableCarsChange(selectedCar, undefined);
            return;
        }
        const relative_path = "product_categories";
        const params = {root_category: this.state.selectedMake};
        fetchFunction(relative_path, this.checkIfModelFetchSuccessful, this.handleFetchError, params);
    };

    checkIfModelFetchSuccessful = (res) => {
        if (Array.isArray(res)) {
            const selectedCar = {make: this.props.availableCars.make, model: res, bodyType: [], year: []};
            this.onAvailableCarsChange(selectedCar, undefined);
        }
        else {
            throw new Error('Cant connect to API or invalid category');
        }
    };

    fetchBodyTypes = () => {
        const {selectedModel} = this.state;
        if (parseInt(selectedModel) === 0) {
            const selectedCar = {make: this.props.availableCars.make, model: this.props.availableCars.model, bodyType: [], year: []};
            this.onAvailableCarsChange(selectedCar, undefined);
            return;
        }
        const relative_path = "product_categories";
        const params = {root_category: this.state.selectedModel};
        fetchFunction(relative_path, this.checkIfBodyTypeFetchSuccessful, this.handleFetchError, params);
    };

    checkIfBodyTypeFetchSuccessful = (res) => {
        if (Array.isArray(res)) {
            const selectedCar = {make: this.props.availableCars.make, model: this.props.availableCars.model, bodyType: res, year: []};
            this.onAvailableCarsChange(selectedCar);
        }
        else {
            throw new Error('Cant connect to API or invalid category');
        }
    };

    fetchYears = () => {
        const {selectedBodyType} = this.state;
        if (parseInt(selectedBodyType) === 0) {
            const selectedCar = {make: this.props.availableCars.make, model: this.props.availableCars.model, bodyType: this.props.availableCars.bodyType, year: []};
            this.onAvailableCarsChange(selectedCar, undefined);
            return;
        }
        const relative_path = "product_categories";
        const params = {root_category: this.state.selectedBodyType};
        fetchFunction(relative_path, this.checkIfYearFetchSuccessful, this.handleFetchError, params);
    };

    checkIfYearFetchSuccessful = (res) => {
        if (Array.isArray(res)) {
            const selectedCar = {make: this.props.availableCars.make, model: this.props.availableCars.model, bodyType: this.props.availableCars.bodyType, year: res};
            this.onAvailableCarsChange(selectedCar);
        }
        else {
            throw new Error('Cant connect to API or invalid category');
        }
    };



    changeMake = (event) => {
        this.setState({selectedMake: event.target.value, selectedModel: 0, selectedBodyType: 0, selectedYear: 0}, () => { this.fetchModels() });

    };

    changeModel = (event) => {
        this.setState({selectedModel: event.target.value, selectedBodyType: 0, selectedYear: 0}, () => {this.fetchBodyTypes() });
    };

    changeBodyType = (event) => {
        this.setState({selectedBodyType: event.target.value, selectedYear: 0}, () => {this.fetchYears()});
    };

    changeYear = (event) => {
        const year = event.target.value;
        this.setState({selectedYear: event.target.value});
        if (parseInt(year) !== 0) {
            const availableCars = {make: this.props.availableCars.make, model: this.props.availableCars.model,
                bodyType: this.props.availableCars.bodyType, year: this.props.availableCars.year};
            this.onAvailableCarsChange(availableCars, parseInt(year));

        }
    };

    render() {
        const {selectedMake, selectedModel, selectedBodyType, selectedYear} = this.state;
        return (
            <section id="main">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <h2>Znajdź odpowiedni hak</h2>
                        <div className="top-form">
                            <span>Marka pojazdu</span>
                            <div className="styled-select">
                                <select value={selectedMake} onChange={this.changeMake}>
                                    <option value={0}>Wybierz markę</option>
                                    {
                                        this.props.availableCars.make.map((item) => (
                                            <option value={item.id} key={item.id}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                            <span>Model pojazdu</span>
                            <div className="styled-select">
                                <select value={selectedModel} onChange={this.changeModel} disabled={parseInt(selectedMake) === 0}>
                                    <option value={0}>Wybierz model</option>
                                    {
                                        this.props.availableCars.model.map((item) => (
                                            <option value={item.id} key={item.id}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                            <span>Typ pojazdu</span>
                            <div className="styled-select">
                                <select value={selectedBodyType} onChange={this.changeBodyType} disabled={parseInt(selectedModel) === 0}>
                                    <option value={0}>Wybierz typ</option>
                                    {
                                        this.props.availableCars.bodyType.map((item) => (
                                            <option value={item.id} key={item.id}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                            <span>Rocznik</span>
                            <div className="styled-select">
                                <select value={selectedYear} onChange={this.changeYear} disabled={parseInt(selectedBodyType) === 0}>
                                    <option value={0}>Wybierz rocznik</option>
                                    {
                                        this.props.availableCars.year.map((item) => (
                                            <option value={item.id} key={item.id}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <Link to='/products'> <div className={"button " + (selectedYear !== 0 ? '' : 'disabled-search-button')}>Wyszukaj</div></Link>
                    </div>
                    <div className="col-lg-6">
                        <img src="/img/towbars-photo.png" className="img-fluid" alt=""/>
                    </div>
                </div>
            </div>
            </section>

        );
    }
    componentDidMount() {
        this.fetchMakes();
    }
}

function mapStateToProps(state) {
    return {
        availableCars: state.availableCarsState,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onAvailableCarsChange: (availableCars, selectedCar) => dispatch(doChangeAvailableCars(availableCars, selectedCar)),
    };
}

const ConnectedTowbarFinder = connect(mapStateToProps, mapDispatchToProps)(TowbarFinder);

export default ConnectedTowbarFinder;

