import React, { Component } from 'react';
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from "../../actions_creators/userSession";
import {Breadcrumbs} from "../shared/Breadcrumbs";
import fetchResourceFromBackEnd from "../../helper_functions/fetcher";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
        }
    }
    linksForBreadcrumbs = () => {
        return [{name: 'Logowanie', path: '/login'}];
    };

    onEmailChange = (event) => {
        this.setState({email: event.target.value});
    };

    onPasswordChange = (event) => {
        this.setState({password: event.target.value});
    };

    signIn = () => {
        const {email, password} = this.state;
        const params = {grant_type: 'password', password: password, email: email};
        if (!email.includes('@') || password.length <= 6) {return;}

        fetchResourceFromBackEnd('oauth/token', this.onSuccessfulRequest, this.onFailedRequest, params, 'POST')
    };

    onSuccessfulRequest = (res) => {
        if (res.hasOwnProperty('error')) {
            this.setState({error: 'Niewłaściwy login lub hasło'});
            return;
        }
        const token = res.access_token;
        this.props.onTokenChange(token);
        this.props.history.push('/');
    };

    onFailedRequest = (err) => {
        this.setState({error: err.message});
    };


    render() {
        const {email, password, error} = this.state;
        return (
            <React.Fragment>
                <Breadcrumbs name='Logowanie' urls={this.linksForBreadcrumbs()}/>
                <section id="content" className="product">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <span>e-mail</span>
                                <input type="text" placeholder="Wpisz e-mail" onChange={this.onEmailChange} value={email}/>
                            </div>
                            <div className="col-lg-12">
                                <span>Hasło</span>
                                <input type="password" placeholder="Wpisz hasło" onChange={this.onPasswordChange} value={password}/>
                            </div>
                            <div className="col-lg-12">
                                <div className='button' onClick={this.signIn}>Zaloguj się</div>
                                {error.length > 1 && <span><br/><strong>{error}</strong></span>}
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        userSession: state.userSessionState,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onTokenChange: (token) => dispatch(doChangeUserSessionToken(token))
    }
}

const ConnectedLogin = connect(mapStateToProps, mapDispatchToProps)(Login);

export default ConnectedLogin;