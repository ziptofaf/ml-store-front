import React, { Component } from 'react';
import fetchResourceFromBackEnd from "../../helper_functions/fetcher";
import MAIN_API_PATH from "../../constants/urls";
import { connect } from 'react-redux';
import {doAddToBasket} from "../../actions_creators/basket";
import {Breadcrumbs} from "../shared/Breadcrumbs";


class ProductShow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: undefined,
            loading: true,
            fetchError: false,
            quantity: 1
        }
    }

    available = () => {
        return this.state.product.available > 0;
    };

    addQuantity = () => {
        const {quantity} = this.state;
        let newQuantity = quantity + 1;
        if (newQuantity > 2) {newQuantity = 2;}
        this.setState({quantity: newQuantity});
    };

    addToBasket = () => {
        const {product, quantity} = this.state;
        this.props.onAddToBasket(product, quantity);
    };

    buyNow = () => {
        const {product, quantity} = this.state;
        this.props.onAddToBasket(product, quantity);
        this.props.history.push('/basket');
    };

    reduceQuantity = () => {
        const {quantity} = this.state;
        let newQuantity = quantity - 1;
        if (newQuantity < 1) {newQuantity = 1;}
        this.setState({quantity: newQuantity});
    };

    linksForBreadcrumbs = () => {
        const {product} = this.state;
        return [{name: 'Produkty', path: '/products'},{name: product.name, path: '/products/'+product.id}];
    };



    grossToNet = () => {
      let price = this.state.product.price;
      return (parseFloat(price) / 1.23).toFixed(2);
    };
    render() {
        const {product, loading, fetchError, quantity} = this.state;
        if (loading) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <h2>
                                Trwa pobieranie produktu, proszę czekać
                            </h2>
                        </div>
                    </div>
                </div>
            );
        }
        if (fetchError) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <h2>
                                Nie udało się pobrać produktu, proszę o odświeżenie strony i o chwilę cierpliwości
                            </h2>
                        </div>
                    </div>
                </div>
            );
        }

        return (
          <React.Fragment>
              <Breadcrumbs name={product.name} urls={this.linksForBreadcrumbs()}/>
              <section id="content" className="product">
                  <div className="container">
                      <div className="row">
                          <div className="col-lg-7">
                              <div className="product-box">
                                  <h4>{product.name}</h4>
                                  <div className="product-photo">
                                      <div className="main-photo">
                                          <img className="img-fluid" src={MAIN_API_PATH+product.image_url} alt="główny obrazek haka"/>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div className="col-lg-5">
                              <div className="product-box">
                                  <div className='price-in-stock'>
                                      <span className='detail-price'>{product.price} zł</span>
                                      <span>  (netto {this.grossToNet()})</span>
                                      <span className={this.available() ? 'stock' : 'stock-unavailable'}>
                                          { this.available() ?
                                              <div className='check'>
                                                  <i className="fas fa-check"/>
                                              </div>
                                              :
                                              <div className='check-unavailable'>
                                                  <i className="fas fa-times"/>
                                              </div>
                                          }
                                          { this.available() ? 'Dostępne' : 'Niedostępne' }
                                      </span>
                                  </div>
                                  { this.available() ?
                                      <div className="row">
                                          <div className="col-lg-4 col-md-4 col-4">
                                              <div className="quantity">
                                                  <span onClick={this.reduceQuantity} className="clickable">-</span>
                                                  <span>{quantity}</span>
                                                  <span onClick={this.addQuantity} className="clickable">+</span>
                                              </div>
                                          </div>
                                          <div className="col-lg-8 col-md-8 col-8">
                                              <div className="button" onClick={this.addToBasket}>Dodaj do koszyka</div>
                                          </div>
                                      </div>
                                      :
                                      <div className="row" style={{padding: '12px'}}> Towar niedostępny </div>
                                  }
                                  { this.available() && <div className="buy" onClick={this.buyNow}>Kup teraz</div>}
                                  <ul className="share-list">
                                      <li><i className="far fa-envelope-open"/><a href={"mailto:shop@tuger.eu?subject=" + product.name}>Zapytaj o produkt</a></li>
                                  </ul>
                              </div>
                              <div className="product-box box-icons">
                                  <div className="row">
                                      <div className="col-lg-4 col-md-4 col-4 line-icon-right">
                                          <div className="product-box-icon">
                                              <i className="fas fa-truck-loading"></i>
                                          </div>
                                          <h6>Szybka dostawa</h6>
                                      </div>
                                      <div className="col-lg-4 col-md-4 col-4 line-icon-right">
                                          <div className="product-box-icon">
                                              <i className="fas fa-undo"></i>
                                          </div>
                                          <h6>100 dni na zwrot</h6>
                                      </div>
                                      <div className="col-lg-4 col-md-4 col-4">
                                          <div className="product-box-icon">
                                              <i className="fas fa-shield-alt"></i>
                                          </div>
                                          <h6>Autoryzowany sprzedawca firmy AutoHak</h6>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div className="col-lg-12">
                               <div className="product-box">
                                   <div id="myTabContent" className="tab-content tab-pane">
                                       DANE TECHNICZNE HAKA<br/>
                                       <p dangerouslySetInnerHTML={{__html: product.description}}/>
                                       {product['plug'] &&
                                           <React.Fragment>
                                               <br/>DANE TECHNICZNE WIĄZKI<br/>
                                               <p dangerouslySetInnerHTML={{__html: product.plug.description}}/>
                                           </React.Fragment>
                                       }
                                   </div>
                               </div>
                          </div>
                      </div>
                  </div>
              </section>
          </React.Fragment>
        );
    }

    componentDidMount() {
        this.fetchProducts();
    }

    fetchProducts = () => {
        const id = this.props.match.params.id;
        const params = {product_type: 'towbar'};
        fetchResourceFromBackEnd('products/'+id , this.onFetchSuccessful, this.onFetchUnsuccessful, params)

    };
    onFetchSuccessful = (res) => {
        if (res['status'] && res['status'] === 'error') {
            throw new Error('Fetching error has occured!');
        }
        this.setState({product: res, loading: false});
    };
    onFetchUnsuccessful = (res) => {
        this.setState({loading: false, fetchError: true});
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAddToBasket: (product, quantity) => dispatch(doAddToBasket(product, quantity)),
    };
}

const ConnectedProductShow = connect(null, mapDispatchToProps)(ProductShow);


export default ConnectedProductShow;