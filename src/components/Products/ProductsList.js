import React, { Component } from 'react';
import { connect } from 'react-redux';
import fetchResourceFromBackEnd from '../../helper_functions/fetcher';
import { Link } from "react-router-dom";
import MAIN_API_PATH from '../../constants/urls';
import {
    arraysCommonPart,
    filterByBallType,
    filterByElectricModule, filterByIndicatorFailure, filterByParkingSensors,
    filterByPlugType
} from "./filters/filteringFunctions";
import {doAddToBasket} from "../../actions_creators/basket";


const Breadcrumbs = () => (
    <section id="breadcrumbs">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h2>Haki holownicze</h2>
                </div>
            </div>
        </div>
    </section>
);

class ProductsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            ballTypesSelected: {fixed: false, horizontal: false, vertical: false},
            plugTypesSelected: {no_plug: false, seven_universal: false, thirteen_universal: false,
                seven_dedicated: false, thirteen_dedicated: false},
            electricModulesSelected: {yes: false, no: false},
            indicatorFailureBulbsSelected: {yes: false, no: false},
            parkingSensorsSwitchesSelected: {yes: false, no: false}
        }
    }
    changeBallType = (type) => {
        let clonedBallTypes = {...this.state.ballTypesSelected};
        clonedBallTypes[type] = !clonedBallTypes[type];
        this.setState({ballTypesSelected: clonedBallTypes});
    };
    changePlugType = (type) => {
        let clonedPlugTypes = {...this.state.plugTypesSelected};
        clonedPlugTypes[type] = !clonedPlugTypes[type];
        this.setState({plugTypesSelected: clonedPlugTypes});
    };
    changeElectricModule = (type) => {
        let clonedElectricModules = {...this.state.electricModulesSelected};
        clonedElectricModules[type] = !clonedElectricModules[type];
        this.setState({electricModulesSelected: clonedElectricModules});
    };
    changeIndicatorFailure = (type) => {
        let clonedIndicators = {...this.state.indicatorFailureBulbsSelected};
        clonedIndicators[type] = !clonedIndicators[type];
        this.setState({indicatorFailureBulbsSelected: clonedIndicators});

    };
    changeParkingSensor = (type) => {
        let clonedSensors = {...this.state.parkingSensorsSwitchesSelected};
        clonedSensors[type] = !clonedSensors[type];
        this.setState({parkingSensorsSwitchesSelected: clonedSensors});
    };
    filterProducts = () => {
        const {ballTypesSelected, plugTypesSelected, electricModulesSelected,
            indicatorFailureBulbsSelected, parkingSensorsSwitchesSelected, products} = this.state;
        const filtered1 = filterByBallType(ballTypesSelected, products);
        const filtered2 = filterByPlugType(plugTypesSelected, products);
        const filtered3 = filterByElectricModule(electricModulesSelected, products);
        const filtered4 = filterByIndicatorFailure(indicatorFailureBulbsSelected, products);
        const filtered5 = filterByParkingSensors(parkingSensorsSwitchesSelected, products);
        return arraysCommonPart(filtered5,arraysCommonPart(filtered4,arraysCommonPart(filtered3,arraysCommonPart(filtered1, filtered2))));
    };
    priceToCurrency = (price) => {
        return price.toString().replace('.', ',') + ' zł'
    };
    render() {
        const {ballTypesSelected, plugTypesSelected, electricModulesSelected,
            indicatorFailureBulbsSelected, parkingSensorsSwitchesSelected} = this.state;
        const filteredProducts = this.filterProducts();
        return (
            <React.Fragment>
                <Breadcrumbs/>
                <section id="content" className="list">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="bg-grey">
                                    <div className="separator"/>

                                    <h4>Typ kuli haka</h4>
                                    <label className="checkbox">Przykręcany na 2 śruby
                                        <input type="checkbox" checked={ballTypesSelected.fixed} onChange={() => this.changeBallType('fixed')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">Zdejmowany horyzontalnie
                                        <input type="checkbox" checked={ballTypesSelected.horizontal} onChange={() => this.changeBallType('horizontal')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">Zdejmowany wertykalnie
                                        <input type="checkbox" checked={ballTypesSelected.vertical} onChange={() => this.changeBallType('vertical')}/>
                                        <span className="checkmark"/>
                                    </label>

                                    <div className="separator"/>

                                    <h4>Typ wiązki w zestawie</h4>
                                    <label className="checkbox">Bez wiązki
                                        <input type="checkbox" checked={plugTypesSelected.no_plug} onChange={() => this.changePlugType('no_plug')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">7 pin uniwersalna
                                        <input type="checkbox" checked={plugTypesSelected.seven_universal} onChange={() => this.changePlugType('seven_universal')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">13 pin uniwersalna
                                        <input type="checkbox" checked={plugTypesSelected.thirteen_universal} onChange={() => this.changePlugType('thirteen_universal')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">7 pin dedykowana
                                        <input type="checkbox" checked={plugTypesSelected.seven_dedicated} onChange={() => this.changePlugType('seven_dedicated')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">13 pin dedykowana
                                        <input type="checkbox" checked={plugTypesSelected.thirteen_dedicated} onChange={() => this.changePlugType('thirteen_dedicated')}/>
                                        <span className="checkmark"/>
                                    </label>

                                    <div className="separator"/>

                                    <h4>Moduł elektryczny</h4>
                                    <label className="checkbox">tak
                                        <input type="checkbox" checked={electricModulesSelected.yes} onChange={() => this.changeElectricModule('yes')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">nie
                                        <input type="checkbox" checked={electricModulesSelected.no} onChange={() => this.changeElectricModule('no')}/>
                                        <span className="checkmark"/>
                                    </label>

                                    <div className="separator"/>

                                    <h4>Sensory parkowania</h4>
                                    <label className="checkbox">tak
                                        <input type="checkbox" checked={parkingSensorsSwitchesSelected.yes} onChange={() => this.changeParkingSensor('yes')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">nie
                                        <input type="checkbox" checked={parkingSensorsSwitchesSelected.no} onChange={() => this.changeParkingSensor('no')}/>
                                        <span className="checkmark"/>
                                    </label>

                                    <div className="separator"/>

                                    <h4>Informacja o uszkodzonej żarówce</h4>
                                    <label className="checkbox">tak
                                        <input type="checkbox" checked={indicatorFailureBulbsSelected.yes} onChange={() => this.changeIndicatorFailure('yes')}/>
                                        <span className="checkmark"/>
                                    </label>
                                    <label className="checkbox">nie
                                        <input type="checkbox" checked={indicatorFailureBulbsSelected.no} onChange={() => this.changeIndicatorFailure('no')} />
                                        <span className="checkmark"/>
                                    </label>
                                </div>
                            </div>




                            <div className="col-lg-9">
                                <div className="row">
                                    <div className="col-lg-10 col-md-10 col-9">
                                    </div>
                                </div>
                                <hr/>
                                <div className="row">
                                    { filteredProducts.map((item) => (
                                        <div className="col-lg-4 col-md-4" key={item.id}>
                                            <div className="box">
                                                <div className="box-photo">
                                                    <Link to={`${this.props.match.url}/${item.id}`}><img className="img-fluid" src={MAIN_API_PATH+item.image_url} alt="Zdjęcie haka"/></Link>
                                                </div>
                                                <h4>{item.name}</h4>
                                                <div dangerouslySetInnerHTML={{__html: item.short_description}}/>
                                                <div className="row">
                                                    <div className="col-lg-6 col-md-6 col-6">
                                                        <p className="price">{this.priceToCurrency(item.price)}</p>
                                                    </div>
                                                    <div className="col-lg-6 col-md-6 col-6">
                                                        <div className="basket-grey">
                                                            { item.available > 0 ?
                                                                <img src="/img/basket-icon.png"
                                                                     alt="Dodaj do koszyka" onClick={() => this.props.onAddToBasket(item)} className="clickable"/>
                                                                :
                                                                <img src="/img/basket-icon-grey.png"
                                                                     alt="Towar niedostępny"/>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
    componentDidMount() {
        this.fetchProducts();
    }

    fetchProducts = () => {
        if (this.props.selectedCar === undefined) {return;}
        const params = {product_category: this.props.selectedCar, product_type: 'towbar'};
        fetchResourceFromBackEnd('products', this.onFetchSuccessful, this.onFetchUnsuccessful, params)

    };
    onFetchSuccessful = (res) => {
        this.setState({products: res});
    };
    onFetchUnsuccessful = (res) => {
        console.log('Nie udało się pobrać produktów, spróbuj ponownie za chwilę');
    }
}

function mapStateToProps(state) {
    return {
        selectedCar: state.availableCarsState.selectedCar,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onAddToBasket: (product) => dispatch(doAddToBasket(product, 1)),
    };
}


const ConnectedProducts = connect(mapStateToProps, mapDispatchToProps)(ProductsList);

export default ConnectedProducts;
