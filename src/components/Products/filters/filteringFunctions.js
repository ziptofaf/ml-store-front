function filterByBallType(filters, products) {
    if (filters.fixed && filters.horizontal && filters.vertical) {return products;}
    if (!filters.fixed && !filters.horizontal && !filters.vertical) {return products;}
    let permittedValues = [];
    let allowedProducts = [];
    if (filters.fixed) {permittedValues.push('fixed')}
    if (filters.horizontal) {permittedValues.push('horizontal')}
    if (filters.vertical) {permittedValues.push('vertical')}

    for (let i =0; i < products.length; i++) {
        let product = products[i];
        for (let j = 0; j < permittedValues.length; j++) {
            let filter = permittedValues[j];
            if (product.ball_type === filter) {
                allowedProducts.push(product);
                break;
            }
        }
    }
    return allowedProducts;
}

function filterByPlugType(filters, products) {
    let permittedValues = [];
    let key = "";
    for (key in filters) {
        let value = filters[key];
        if (value) {permittedValues.push(key)}
    }

    if (permittedValues.length === 0) {return products;}
    if (permittedValues.length === 5) {return products;}
    return products.filter((item) => (permittedValues.includes(mapProductToPlugType(item))));
}

function mapProductToPlugType(product) {
    if (!('plug' in product)) {return 'no_plug'}
    const plug = product.plug;
    if (plug.socket === 7) {
        if (plug.dedicated) {return 'seven_dedicated';}
        return 'seven_universal';
    }
    if (plug.dedicated) {return 'thirteen_dedicated';}
    return 'thirteen_universal';
}

function arraysCommonPart(array1, array2) {
    return array1.filter(value => array2.includes(value));
}

function filterByElectricModule(filters, products) {
    return yesNoPlugFilter(filters, products, 'bypass_relay');
}

function filterByParkingSensors(filters, products) {
    return yesNoPlugFilter(filters, products, 'parking_sensors_switch');
}

function filterByIndicatorFailure(filters, products) {
    return yesNoPlugFilter(filters, products, 'indicator_failure_bulb');
}

// general filter for values that take 2 values (yes, no)
function yesNoPlugFilter(filters, products, objectKey) {
    let permittedValues = [];
    let key = "";
    for (key in filters) {
        let value = filters[key];
        if (value) {permittedValues.push(key)}
    }
    if (permittedValues.length === 0 || permittedValues.length === 2) {return products;}
    const soughtValue = permittedValues[0] === 'yes';
    console.log(products[0]);
    return products.filter(item => (item['plug'] && item['plug'][objectKey] === soughtValue));
}

export {filterByBallType, filterByPlugType, filterByElectricModule, filterByIndicatorFailure,
    filterByParkingSensors, arraysCommonPart}