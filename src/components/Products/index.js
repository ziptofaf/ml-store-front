import React, { Component } from 'react';
import { Route } from "react-router-dom";
import ProductsList from './ProductsList';
import ProductShow from './ProductShow';


class ProductsWrapper extends Component {
    render() {
        const match = this.props.match;
        console.log(match);
        return (
          <React.Fragment>
              <Route path={`${match.path}/:id`} component={ProductShow} />
              <Route exact path={match.path} component={ProductsList}/>
          </React.Fragment>
        );
    }
}

export default ProductsWrapper;