import React  from 'react';


const ErrorsList = ({errors}) => (
    <React.Fragment>
        <p>Następujące błędy nie pozwoliły na dokonanie rejestracji:<br/></p>
        <div className="container">
            <ul className="list-group">
                {Object.keys(errors).map((item) => (
                    <li className="list-group-item" key={item}>{item}:
                        {errors[item].map((err) => (
                            <strong>{err},</strong>
                        ))}
                    </li>
                ))}
            </ul>
        </div>
    </React.Fragment>
);

export {ErrorsList}