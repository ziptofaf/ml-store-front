import React, { Component } from 'react';
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from "../../actions_creators/userSession";
import {Breadcrumbs} from "../shared/Breadcrumbs";
import fetchResourceFromBackEnd from "../../helper_functions/fetcher";
import {ErrorsList} from "./ErrorsList";

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            passwordConfirmation: '',
            registrationErrors: undefined,
            registrationFinished: false,
        }
    }

    linksForBreadcrumbs = () => {
        return [{name: 'Rejestracja', path: '/register'}];
    };

    validEmail = () => {
        return (this.state.email.includes('@'));
    };
    validPassword = () => {
        return (this.state.password.length >= 6 && this.state.password === this.state.passwordConfirmation)
    };

    validParams = () => {
        return (this.validEmail() && this.validPassword())
    };

    signUp = () => {
        const {email, password, passwordConfirmation} = this.state;
        if (!this.validParams()) {return}
        const params = {user: {email: email, password: password, password_confirmation: passwordConfirmation}};
        fetchResourceFromBackEnd('users', this.onSuccessfulRequest, this.onFailedRequest, params, 'POST');
    };

    onSuccessfulRequest = (res) => {
        if (res.hasOwnProperty('errors')) {
            this.setState({registrationErrors: res.errors});
            return;
        }
        this.setState({registrationFinished: true});
        this.props.history.push('/login');
    };

    onFailedRequest = (err) => {
        const errorMessages = {general: [err.message]};
        this.setState({registrationErrors: errorMessages});

    };

    onEmailChange = (event) => {
      this.setState({email: event.target.value});
    };

    onPasswordChange = (event) => {
        this.setState({password: event.target.value});
    };

    onPasswordConfirmationChange = (event) => {
        this.setState({passwordConfirmation: event.target.value});
    };

    render() {
        const {email, password, passwordConfirmation, registrationErrors} = this.state;
        return (
          <React.Fragment>
              <Breadcrumbs name='Rejestracja' urls={this.linksForBreadcrumbs()}/>
              <section id="content" className="product">
                  <div className="container">
                      <div className="row">
                          <div className="col-lg-12">
                              <span>e-mail</span>
                              <input type="text" placeholder="Wpisz e-mail" onChange={this.onEmailChange} value={email}/>
                              {email.length > 2 && !this.validEmail() && <span>Musisz wpisać poprawny adres e-mail</span>}
                          </div>
                          <div className="col-lg-12">
                              <span>Hasło</span>
                              <input type="password" placeholder="Wpisz hasło (minimum 6 znaków)" onChange={this.onPasswordChange} value={password}/>
                          </div>
                          <div className="col-lg-12">
                              <span>Wpisz hasło jeszcze raz</span>
                              <input type="password" placeholder="Wpisz hasło" onChange={this.onPasswordConfirmationChange} value={passwordConfirmation}/>
                              {password.length > 2 && password !== passwordConfirmation && <span>Poprzednio wpisane hasło nie jest identyczne z tym</span>}
                          </div>
                          <div className="col-lg-12">
                              <div className={this.validParams() ? 'button' : 'button disabled-search-button'} onClick={this.signUp}>Zarejestruj się</div>
                          </div>
                          {registrationErrors !== undefined && <ErrorsList errors={registrationErrors}/>}
                      </div>
                  </div>
              </section>
          </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        userSession: state.userSessionState,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onTokenChange: (token) => dispatch(doChangeUserSessionToken(token))
    }
}

const ConnectedRegistration = connect(mapStateToProps, mapDispatchToProps)(Registration);

export default ConnectedRegistration;