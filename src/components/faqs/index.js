export {default as Questions} from './partials/Questions';
export {default as Returns} from './partials/Returns';
export {default as Terms} from './partials/Terms';
export {default as Privacy} from './partials/Privacy';