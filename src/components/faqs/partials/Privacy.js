import React from 'react';

import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Polityka prywatności', path: '/privacy'}]
}

const privacy = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Polityka prywatności'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <p className="center document-heading center">Polityka prywatności Sklepu Tuger - haki samochodowe
                            https://tuger.pl
                            ("Sklep") </p>
                        <section><strong className="color-blue">Drogi Użytkowniku!</strong>

                            <p>
                                Dbamy o Twoją prywatność i chcemy, abyś w czasie korzystania z naszych usług czuł się
                                komfortowo. Dlatego też
                                poniżej prezentujemy Ci najważniejsze informacje o zasadach przetwarzania przez nas
                                Twoich danych osobowych oraz
                                plikach cookies, które są wykorzystywane przez nasz Sklep.
                                Informacje te zostały przygotowane z uwzględnieniem RODO, czyli ogólnego rozporządzenia
                                o ochronie danych.
                            </p>
                        </section>
                        <section><h2 className="title-with-border">
                            ADMINISTRATOR DANYCH OSOBOWYCH ("Administrator")
                        </h2>

                            ML SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą pod adresem ul. Marszałka Józefa
                            Piłsudskiego 30, 75-511 Koszalin,
                            wpisaną do Krajowego Rejestru Sądowego - rejestru przedsiębiorców przez SĄD REJONOWY W
                            KOSZALINIE, IX WYDZIAŁ GOSPODARCZY KRAJOWEGO REJESTRU SĄDOWEGO, pod nr KRS 0000519814,
                            NIP 6692522107, nr REGON 32155789500000, kapitał zakładowy 5000,00 zł .

                        </section>
                        <section><h2 className="title-with-border">
                            DANE OSOBOWE I PRYWATNOŚĆ
                        </h2>
                            <p>
                                Jeśli zamierzasz założyć Konto Użytkownika oraz korzystać z naszych usług, zostaniesz
                                poproszony o podanie nam Twoich danych osobowych.
                                <br/>
                                Twoje dane przetwarzane są przez nas we wskazanych poniżej celach, związanych z
                                funkcjonowaniem Sklepu i
                                świadczeniem usług w nim oferowanych („Usługi”).
                            </p>

                            <p className="color-blue">
                                <strong>Cel przetwarzania:</strong>
                            </p>

                            <p>
                                W zależności od tego, na co się zdecydujesz, może to być:
                            </p>
                            <ul>
                                <li>Świadczenie usług oferowanych w Sklepie</li>
                                <li>Realizacja Twoich zamówień</li>
                                <li>Marketing bezpośredni oferowanych usług, inny niż newsletter</li>
                            </ul>
                            <p className="color-blue">
                                <strong>Podstawa przetwarzania:</strong>
                            </p>

                            <ul>
                                <li>
                                    Umowa sprzedaży lub działania podejmowane na Twoje żądanie, zmierzające do jej
                                    zawarcia (art. 6 ust. 1 lit. b RODO)
                                </li>
                                <li>
                                    Ciążący na nas obowiązek prawny, np. związany z rachunkowością (art. 6 ust. 1 lit. c
                                    RODO)
                                </li>

                                <li>
                                    Umowa o świadczenie usług lub działania podejmowane na Twoje żądanie, zmierzające do
                                    jej zawarcia (art. 6 ust. 1 lit. b RODO)
                                </li>
                                <li>
                                    Nasz prawnie uzasadniony interes, polegający na przetwarzaniu danych w celu
                                    ustalenia, dochodzenia lub obrony ewentualnych roszczeń (art. 6 ust. 1 lit. f RODO)
                                </li>
                                <li>
                                    Nasz prawnie uzasadniony interes, polegający na prowadzeniu marketingu
                                    bezpośredniego (art. 6 ust. 1 lit. f RODO)
                                </li>
                                <li>
                                    Nasz prawnie uzasadniony interes polegający na przetwarzaniu danych w celach
                                    analitycznych i statystycznych (art. 6 ust. 1 lit. f RODO)
                                </li>
                                <li>
                                    Nasz prawnie uzasadniony interes polegający na badaniu satysfakcji klientów (art. 6
                                    ust. 1 lit. f RODO)
                                </li>
                            </ul>
                            <p className="color-blue">
                                <strong>Podanie danych:</strong>
                            </p>

                            <ul>
                                <li>Dobrowolne, ale w niektórych przypadkach może być konieczne do zawarcia umowy.</li>
                            </ul>
                            <p className="color-blue">
                                <strong>Skutek niepodania danych:</strong>
                            </p>
                            <p>
                                W zależności od celu, w jakim dane są podawane:
                            </p>
                            <ul>
                                <li>brak możliwości zarejestrowania się w Sklepie</li>
                                <li>brak możliwości korzystania z usług Sklepu</li>
                                <li>brak możliwości dokonania zakupów w Sklepie</li>

                            </ul>
                        </section>
                        <section><h2 className="title-with-border">
                            OKRES PRZETWARZANIA
                        </h2>

                            <p>
                                Twoje dane będziemy przetwarzać tylko przez okres, w którym będziemy mieć ku temu
                                podstawę prawną, a więc do momentu, w którym:
                            </p>
                            <ul>
                                <li>
                                    przestanie ciążyć na nas obowiązek prawny, zobowiązujący nas do przetwarzania Twoich
                                    danych
                                </li>
                                <li>
                                    ustanie możliwość ustalania, dochodzenia lub obrony ewentualnych roszczeń związanych
                                    z umową zawartą przez Sklep przez strony
                                </li>
                                <li>
                                    zostanie przyjęty Twój sprzeciw wobec przetwarzania Twoich danych osobowych - w
                                    przypadku gdy podstawą przetwarzania Twoich danych był uzasadniony interes
                                    administratora lub gdy dane były przetwarzane w celu marketingu bezpośredniego
                                </li>
                            </ul>
                            <p>
                                - w zależności od tego, co ma zastosowanie w danym przypadku i co nastąpi najpóźniej.
                            </p>

                        </section>
                        <section><h2 className="title-with-border">
                            BEZPIECZEŃSTWO DANYCH
                        </h2>
                            <p>
                                Przetwarzając Twoje dane osobowe stosujemy środki organizacyjne i techniczne zgodne z
                                właściwymi przepisami
                                prawa, w tym stosujemy szyfrowanie połączenia za pomocą certyfikatu SSL. </p>
                        </section>
                        <section><h2 className="title-with-border">
                            TWOJE UPRAWNIENIA
                        </h2>
                            <p>
                                Przysługuje Ci prawo żądania:
                            </p>
                            <ul>
                                <li>
                                    dostępu do Twoich danych osobowych,
                                </li>
                                <li>
                                    ich sprostowania,
                                </li>
                                <li>
                                    usunięcia,
                                </li>
                                <li>
                                    ograniczenia przetwarzania,
                                </li>
                                <li>
                                    żądania przeniesienia danych do innego administratora,
                                </li>
                            </ul>
                            <p>A także:</p>
                            <ul>
                                <li>
                                    wniesienia w dowolnym momencie sprzeciwu wobec przetwarzania Twoich danych:
                                    <ul>
                                        <li>
                                            z przyczyn związanych z Twoją szczególną sytuacją – wobec przetwarzania
                                            dotyczących Ciebie danych osobowych, opartego na art. 6 ust. 1 lit. f RODO
                                            (tj. na prawnie uzasadnionych interesach realizowanych przez
                                            administratora),
                                        </li>
                                        <li>
                                            jeżeli dane osobowe są przetwarzane na potrzeby marketingu bezpośredniego, w
                                            zakresie, w jakim przetwarzanie jest związane z takim marketingiem
                                            bezpośrednim.
                                        </li>
                                    </ul></li>
                            </ul>
                            <p>Skontaktuj się z nami, jeśli chcesz skorzystać ze swoich praw.</p>
                            <p>
                                Jeśli uznasz, że Twoje dane są przetwarzane niezgodnie z prawem, możesz złożyć skargę do
                                organu nadzorczego.
                            </p>
                        </section>
                        <section><h2 className="title-with-border">
                            CIASTECZKA
                        </h2>
                            <p>
                                Nasz Sklep, jak większość witryn internetowych, korzysta z tzw. plików cookies
                                (ciasteczek). Pliki te:
                            </p>
                            <ul>
                                <li>są zapisywane w pamięci Twojego urządzenia (komputera, telefonu, itd.)</li>
                                <li>umożliwiają Ci, m.in., korzystanie ze wszystkich funkcji Sklepu</li>
                                <li>nie powodują zmian w ustawieniach Twojego urządzenia</li>
                            </ul>
                            <p>
                                Korzystając z odpowiednich opcji Twojej przeglądarki, w każdej chwili możesz:
                            </p>
                            <ul>
                                <li>usunąć pliki cookies</li>
                                <li>blokować wykorzystanie plików cookies w przyszłości</li>
                            </ul>
                            <p>
                                W tym Sklepie ciasteczka wykorzystywane są w celu:
                            </p>
                            <ul>
                                <li>
                                    zapamiętywania informacji o Twojej sesji
                                </li>
                                <li>
                                    statystycznym
                                </li>
                                <li>
                                    marketingowym
                                </li>
                                <li>
                                    udostępniania funkcji Sklepu
                                </li>
                            </ul>
                            <p>
                                Aby dowiedzieć się jak zarządzać plikami cookies, w tym jak wyłączyć ich obsługę w
                                Twojej przeglądarce, możesz skorzystać z
                                pliku pomocy Twojej przeglądarki. Z informacjami na ten temat możesz zapoznać się
                                wciskając klawisz F1 w przeglądarce. Ponadto
                                odpowiednie wskazówki znajdziesz na następujących podstronach, w zależności od
                                przeglądarki, której używasz:
                            </p>
                            <ul>
                                <li><a
                                    href="https://support.mozilla.org/pl/kb/W%C5%82%C4%85czanie%20i%20wy%C5%82%C4%85czanie%20obs%C5%82ugi%20ciasteczek"
                                    rel="noopener noreferrer" target="_blank">Firefox</a></li>
                                <li><a href="https://support.google.com/chrome/answer/95647?hl=plX" rel="noopener noreferrer"
                                       target="_blank">Chrome</a></li>
                                <li><a href="https://support.apple.com/kb/PH5042?locale=en_US" rel="noopener noreferrer"
                                       target="_blank">Safari</a></li>
                                <li><a href="http://windows.microsoft.com/pl-pl/windows-10/edge-privacy-faq"
                                       rel="noopener noreferrer" target="_blank">Internet Explorer / Microsoft Edge</a></li>
                            </ul>
                            <p>
                                Więcej informacji o ciasteczkach znajdziesz w <a
                                href="https://pl.wikipedia.org/wiki/HTTP_cookie" rel="noopener noreferrer"
                                target="_blank">Wikipedii</a>.
                            </p>
                        </section>
                        <section><h2 className="title-with-border">
                            USŁUGI ZEWNĘTRZNE / ODBIORCY DANYCH
                        </h2>
                            <p>
                                Korzystamy z usług podmiotów zewnętrznych, którym mogą być przekazywane Twoje dane.
                                Poniżej znajduje się lista możliwych odbiorców Twoich danych:
                            </p>
                            <ul>
                                <li>
                                    dostawca oprogramowania potrzebnego do prowadzenia sklepu internetowego
                                </li>


                                <li>
                                    podmiot realizujący dostawę towarów
                                </li>


                                <li>
                                    dostawca płatności
                                </li>


                                <li>
                                    biuro księgowe
                                </li>


                                <li>
                                    osoby współpracujące z nami na podstawie umów cywilnoprawnych, wspierające naszą
                                    bieżącą działalność
                                </li>


                                <li>
                                    podmiot zapewniający nam wsparcie techniczne
                                </li>


                                <li>
                                    podmiot zapewniający system mailingowy
                                </li>


                                <li>
                                    odpowiednie organy publiczne w zakresie, w jakim Administrator jest zobowiązany do
                                    udostępnienia im danych
                                </li>
                            </ul>
                        </section>
                        <section><h2 className="title-with-border">
                            KONTAKT Z ADMINISTRATOREM
                        </h2>
                            <p>
                                Chcesz skorzystać ze swoich uprawnień dotyczących danych osobowych?
                            </p>
                            <p>
                                A może po prostu chcesz zapytać o coś związanego z naszą Polityką Prywatności?
                            </p>
                            <p>
                                Napisz na adres e-mail:<br/>
                                shop@tuger.eu </p>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default privacy;