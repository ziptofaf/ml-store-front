import React from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Zwroty i reklamacje', path: '/returns'}]
}

const returns = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Zwroty i reklamacje'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <p className="center document-heading center">Odstąpienie od umowy</p>
                        <p>
                            Jeśli jesteś konsumentem - co do zasady przysługuje Ci <strong>prawo do odstąpienia od zawartej na odległość umowy</strong>.
                        </p>

                        <p>
                            Od umowy możesz odstąpić, składając nam <strong>oświadczenie o odstąpieniu od umowy</strong> np. poprzez wiadomość e-mail przesłaną na adres: <a href="mailto:shop@tuger.eu">shop@tuger.eu</a>
                        </p>
                        <p>
                            <strong>Zwracaną rzecz zwróć do nas niezwłocznie na adres:
                                Ml sp. z o.o Głobino 74 76-200 Słupsk           </strong>, jednak nie później niż w terminie 14 dni od dnia, w którym odstąpiłeś od umowy.
                        </p>
                        <p>
                            Od umowy możesz odstąpić w terminie <strong>14 dni</strong>.
                        </p>
                        <p>
                            Bieg czternastodniowego terminu na odstąpienie od umowy rozpoczyna się:
                        </p>
                        <ul><li>
                            dla umowy, w której wydajemy rzecz, będąc zobowiązanymi do przeniesienia jej własności - <strong>od objęcia rzeczy w posiadanie przez Ciebie</strong> lub wskazaną przez Ciebie osobę trzecią inną niż przewoźnik,
                        </li>
                            <li>
                                dla treści cyfrowych - od dnia zawarcia umowy.
                            </li>
                        </ul><p>
                        Do zachowania terminu wystarczy wysłanie oświadczenia przed jego upływem.
                    </p>
                        <p>
                            Dokonane przez Ciebie płatności, w tym koszty dostarczenia rzeczy na zasadach wskazanych w regulaminie zwrócimy Ci niezwłocznie, jednak nie później niż w terminie 14 dni od dnia otrzymania od Ciebie oświadczenia o odstąpieniu od umowy.
                        </p>
                        <p>
                            Możemy wstrzymać się ze zwrotem otrzymanej od Ciebie płatności do chwili otrzymania rzeczy z powrotem lub dostarczenia przez Ciebie dowodu jej odesłania, w zależności od tego, które zdarzenie nastąpi wcześniej.
                        </p>
                        <p>
                            <strong>
                                Pamiętaj, że prawo odstąpienia od umowy zawartej na odległość nie przysługuje konsumentowi m.in. w odniesieniu do umowy:
                            </strong>
                        </p>
                        <ul><li>
                            w której przedmiotem świadczenia jest <strong>rzecz nieprefabrykowana, wyprodukowana według specyfikacji konsumenta</strong> lub <strong>służąca zaspokojeniu jego zindywidualizowanych potrzeb</strong>,
                        </li>
                            <li>
                                w której przedmiotem świadczenia jest <strong>rzecz ulegająca szybkiemu zepsuciu</strong> lub mająca krótki termin przydatności do użycia,
                            </li>
                            <li>
                                w której przedmiotem świadczenia jest rzecz dostarczana w zapieczętowanym opakowaniu, której <strong>po otwarciu opakowania nie można zwrócić ze względu na ochronę zdrowia lub ze względów higienicznych</strong>, jeżeli opakowanie zostało otwarte po dostarczeniu,
                            </li>
                            <li>
                                w której przedmiotem świadczenia są <strong>nagrania dźwiękowe lub wizualne albo programy komputerowe dostarczane w zapieczętowanym opakowaniu</strong>, jeżeli opakowanie zostało otwarte po dostarczeniu
                            </li>
                            <li>
                                <strong>o dostarczanie treści cyfrowych, które nie są zapisane na nośniku materialnym</strong>, jeżeli spełnianie świadczenia rozpoczęło się za wyraźną zgodą konsumenta przed upływem terminu do odstąpienia od umowy i po poinformowaniu go przez przedsiębiorcę o utracie prawa odstąpienia od umowy.
                            </li>
                        </ul><p>
                        Szczegółowe informacje na temat odstąpienia od umowy znajdziesz w naszym Regulaminie sprzedaży.
                    </p>
                        <p className="center document-heading center">Reklamacje</p>
                        <p>
                            W przypadku gdy zakupiony u nas towar okaże się wadliwy, masz prawo reklamować go w oparciu o rękojmię. Reklamację najlepiej złożyć na adres e-mail: <a href="mailto:shop@tuger.eu">shop@tuger.eu</a> lub drogą pocztową na adres             ul. Marszałka Józefa Piłsudskiego 30, 75-511 Koszalin        .
                        </p>


                        <p>
                            <strong>Reklamowany towar należy wysłać na adres:
                                Ml sp. z o.o Głobino 74 76-200 Słupsk        </strong>
                        </p>

                        <p>
                            W związku z wadą możesz żądać:
                        </p>
                        <ul><li>
                            <strong>wymiany rzeczy na wolną od wad,</strong>
                        </li>
                            <li>
                                <strong>usunięcia wady,</strong>
                            </li>
                        </ul><p>
                        lub złożyć oświadczenie o:
                    </p>
                        <ul><li>
                            <strong>obniżeniu ceny,</strong>
                        </li>
                            <li>
                                <strong>odstąpieniu od umowy – w przypadku wady istotnej.</strong>
                            </li>
                        </ul><p>
                        Odpowiadamy względem Ciebie za wady stwierdzone przed upływem dwóch lat od dnia wydania rzeczy.
                    </p>
                        <p>
                            Co do zasady reklamację możesz <strong>zgłosić w ciągu roku od zauważenia wady</strong>, ale czas na złożenie reklamacji nie może się zakończyć przed upływem okresu odpowiedzialności sprzedawcy.
                        </p>
                        <p>
                            <strong>
                                Rozpatrzenie Twojej reklamacji (ustosunkowanie się do niej) nastąpi w ciągu 14 dni od daty otrzymania przez nas Twojego zgłoszenia reklamacyjnego.
                            </strong>
                        </p>
                        <p>
                            Więcej informacji na temat reklamacji znajdziesz w naszym Regulaminie sprzedaży.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default returns;