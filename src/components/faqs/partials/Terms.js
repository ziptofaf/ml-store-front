import React from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Regulamin', path: '/terms'}]
}

const terms = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Regulamin'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">

                        <div className="text"><h1 className="document-heading center">Regulamin sklepu internetowego Tuger -
                            haki samochodowe</h1>

                            <p className="center">
                                Określający m.in. zasady zawierania umów sprzedaży poprzez sklep, zawierający najważniejsze
                                informacje o Sprzedawcy, sklepie oraz o prawach Konsumenta.
                            </p>

                            <p>
                                <b>SPIS TREŚCI</b><br/><b>§ 1</b> Definicje<br/><b>§ 2</b> Kontakt ze Sprzedawcą<br/><b>§
                                3</b> Wymogi techniczne<br/><b>§ 4</b> Zakupy w Sklepie<br/><b>§ 5</b> Płatności<br/><b>§
                                6</b> Realizacja zamówienia<br/><b>§ 7</b> Prawo odstąpienia od umowy<br/><b>§ 8</b> Wyjątki
                                od prawa odstąpienia od umowy<br/><b>§ 9</b> Reklamacje<br/><b>§ 10</b> Dane osobowe<br/><b>§
                                    11</b> Zastrzeżenia<br/><b>Załącznik nr 1:</b> Wzór formularza odstąpienia od umowy
                            </p>

                            <h2>§ 1 DEFINICJE</h2>

                            <b>Dni robocze</b> – dni od poniedziałku do piątku za wyjątkiem dni ustawowo wolnych od
                            pracy.<br/><b>Konto</b> – uregulowana odrębnym regulaminem nieodpłatna funkcja Sklepu (usługa
                                świadczona drogą elektroniczną), dzięki której Kupujący może założyć w Sklepie swoje
                                indywidualne Konto.<br/><b>Konsument</b> – Konsument w rozumieniu przepisów Kodeksu
                                    cywilnego.<br/><b>Kupujący</b> - każdy podmiot Kupujący w Sklepie.<br/><b>Regulamin</b> –
                                        niniejszy regulamin.<br/><b>Sklep</b> - sklep internetowy Tuger - haki samochodowe
                                            prowadzony przez Sprzedawcę pod adresem <a href="https://tuger.pl"
                                                                                       rel="noopener noreferrer"
                                                                                       target="_blank">https://tuger.pl</a>.<br/><b>Sprzedawca</b> -
                                                ML SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą pod adresem ul.
                                                Marszałka Józefa Piłsudskiego 30, 75-511 Koszalin,
                                                wpisaną do Krajowego Rejestru Sądowego - rejestru przedsiębiorców przez SĄD
                                                REJONOWY W KOSZALINIE, IX WYDZIAŁ GOSPODARCZY KRAJOWEGO REJESTRU SĄDOWEGO,
                                                pod nr KRS 0000519814,
                                                NIP 6692522107, nr REGON 32155789500000, kapitał zakładowy 5000,00 zł.

                                                <h2>§ 2 KONTAKT ZE SPRZEDAWCĄ</h2>

                                                <ol>
                                                    <li>
                                                        Adres pocztowy:
                                                        ul. Marszałka Józefa Piłsudskiego 30, 75-511 Koszalin
                                                    </li>
                                                    <li>
                                                        Adres e-mail: shop@tuger.eu
                                                    </li>
                                                    <li>
                                                        Telefon: 792-001-245
                                                    </li>

                                                    <li>
                                                        Adres do zwrotu towaru (w przypadku odstąpienia od umowy): Ml sp. z
                                                        o.o Głobino 74 76-200 Słupsk.
                                                    </li>

                                                    <li>
                                                        Adres do wysłania reklamowanego towaru: Ml sp. z o.o Głobino 74
                                                        76-200 Słupsk.
                                                    </li>


                                                </ol>
                                                <h2>§ 3 WYMOGI TECHNICZNE</h2>

                                                <ol>
                                                    <li>
                                                        Dla prawidłowego funkcjonowania Sklepu potrzebne jest:
                                                        <ul>
                                                            <li>Urządzenie z dostępem do Internetu</li>
                                                            <li>Przeglądarka internetowa obsługująca JavaScript oraz pliki
                                                                cookies.
                                                            </li>
                                                        </ul></li>
                                                    <li>Dla złożenia zamówienia w Sklepie, poza wymogami określonymi w ust.
                                                        1, niezbędne jest aktywne konto e-mail.
                                                    </li>
                                                </ol>
                                                <h2>§ 4 ZAKUPY W SKLEPIE</h2>

                                                <ol>
                                                    <li>Ceny towarów widoczne w Sklepie są całkowitymi cenami za towar, w
                                                        tym zawierają podatek VAT.
                                                    </li>
                                                    <li>Sprzedawca zwraca uwagę, że na całkowitą cenę zamówienia składają
                                                        się wskazane w Sklepie: cena za towar oraz, jeśli w danym przypadku
                                                        ma to zastosowanie, koszty dostawy towaru.
                                                    </li>
                                                    <li>Wybrany do kupienia towar należy dodać do koszyka w Sklepie.</li>
                                                    <li>Następnie Kupujący wybiera z dostępnych w Sklepie: sposób dostawy
                                                        towaru oraz metodę płatności za zamówienie, a także podaje dane
                                                        niezbędne do zrealizowania złożonego zamówienia.
                                                    </li>
                                                    <li>Zamówienie zostaje złożone w momencie potwierdzenia jego treści i
                                                        zaakceptowania Regulaminu przez Kupującego.
                                                    </li>
                                                    <li>Złożenie zamówienia jest tożsame z zawarciem umowy sprzedaży
                                                        pomiędzy Kupującym a Sprzedawcą.
                                                    </li>
                                                    <li>Sprzedawca przekaże Konsumentowi potwierdzenie zawarcia umowy
                                                        sprzedaży na trwałym nośniku najpóźniej w momencie dostarczenia
                                                        towaru.
                                                    </li>
                                                    <li>
                                                        Kupujący może zarejestrować się w Sklepie tj. założyć w nim Konto
                                                        lub dokonywać zakupów bez rejestracji poprzez podawanie swoich
                                                        danych przy każdym ewentualnym zamówieniu.
                                                    </li>
                                                </ol>
                                                <h2>§ 5 PŁATNOŚCI</h2>

                                                <ol>
                                                    <li>
                                                        Za złożone zamówienie można zapłacić, w zależności od wyboru
                                                        Kupującego:
                                                        <ol>
                                                            <li>
                                                                Za pośrednictwem platformy płatniczej:

                                                                <ul>
                                                                    <li>Płatności Shoper</li>
                                                                    <li>PayU</li>
                                                                </ul></li>
                                                            <li>
                                                                Za pobraniem, tj.
                                                                gotówką
                                                                w momencie dostarczenia towaru do Kupującego.
                                                            </li>
                                                        </ol></li>
                                                    <li>
                                                        W przypadku wybrania płatności za pośrednictwem platformy płatniczej
                                                        Płatności Shoper, podmiotem świadczącym obsługę płatności online
                                                        jest Blue Media S.A.
                                                    </li>
                                                    <li>W przypadku wybrania przez Kupującego płatności z góry, za
                                                        zamówienie należy zapłacić w terminie 5 Dni roboczych od złożenia
                                                        zamówienia.
                                                    </li>
                                                    <li>Sprzedawca informuje, że w przypadku niektórych metod płatności, ze
                                                        względu na ich specyfikę, opłacenie zamówienia tą metodą jest
                                                        możliwe wyłącznie bezpośrednio po złożeniu zamówienia.
                                                    </li>
                                                    <li>Kupujący dokonując zakupów w Sklepie akceptuje stosowanie faktur
                                                        elektronicznych przez Sprzedawcę. Kupujący ma prawo wycofać swoją
                                                        akceptację.
                                                    </li>
                                                </ol>
                                                <h2>§ 6 REALIZACJA ZAMÓWIENIA</h2>

                                                <ol>
                                                    <li>Sprzedawca jest obowiązany do dostarczenia towaru bez wad.</li>
                                                    <li>
                                                        Termin realizacji zamówienia wskazany jest w Sklepie.
                                                    </li>
                                                    <li>W przypadku, gdy Kupujący wybrał płatność z góry za zamówienie,
                                                        Sprzedawca przystąpi do realizacji zamówienia po jego opłaceniu.
                                                    </li>
                                                    <li>W sytuacji, gdy w ramach jednego zamówienia Kupujący zakupił towary
                                                        o różnym terminie realizacji, zamówienie zostanie zrealizowane w
                                                        terminie właściwym dla towaru o najdłuższym terminie.
                                                    </li>
                                                    <li>
                                                        Towar dostarczany jest wyłącznie na terytorium Rzeczypospolitej
                                                        Polskiej.
                                                    </li>
                                                    <li>
                                                        Towary zakupione w Sklepie dostarczane są w zależności od tego jaką
                                                        metodę dostawy wybrał Kupujący:
                                                        <ol>
                                                            <li>Za pośrednictwem firmy kurierskiej</li>
                                                        </ol></li>
                                                </ol>
                                                <h2>§ 7 PRAWO ODSTĄPIENIA OD UMOWY</h2>

                                                <ol>
                                                    <li>Konsument ma prawo odstąpić od umowy zawartej ze Sprzedawcą za
                                                        pośrednictwem Sklepu, z zastrzeżeniem § 8 Regulaminu, w terminie 14
                                                        dni bez podania jakiejkolwiek przyczyny.
                                                    </li>
                                                    <li>
                                                        Termin do odstąpienia od umowy wygasa po upływie 14 dni od dnia:
                                                        <ol>
                                                            <li>W którym Konsument wszedł w posiadanie towaru lub w którym
                                                                osoba trzecia inna niż przewoźnik i wskazana przez
                                                                Konsumenta weszła w posiadanie tego towaru.
                                                            </li>
                                                            <li>W którym Konsument wszedł w posiadanie ostatniej z rzeczy
                                                                lub w którym osoba trzecia, inna niż przewoźnik i wskazana
                                                                przez Konsumenta, weszła w posiadanie ostatniej z rzeczy w
                                                                przypadku umowy zobowiązującej do przeniesienia własności
                                                                wielu rzeczy, które dostarczane są osobno.
                                                            </li>
                                                        </ol></li>
                                                    <li>Aby Konsument mógł skorzystać z prawa odstąpienia od umowy musi
                                                        poinformować Sprzedawcę, korzystając z danych podanych w § 2
                                                        Regulaminu, o swojej decyzji o odstąpieniu od umowy w drodze
                                                        jednoznacznego oświadczenia (na przykład pismo wysłane pocztą lub
                                                        informacja przekazana pocztą elektroniczną).
                                                    </li>
                                                    <li>Konsument może skorzystać z wzoru formularza odstąpienia od umowy
                                                        umieszczonego na końcu Regulaminu, jednak nie jest to obowiązkowe.
                                                    </li>
                                                    <li>
                                                        Aby zachować termin do odstąpienia od umowy wystarczy, że Konsument
                                                        wyśle informację dotyczącą wykonania przysługującego mu prawa
                                                        odstąpienia od umowy przed upływem terminu do odstąpienia od umowy.
                                                        <br/><br/><b>SKUTKI ODSTĄPIENIA OD UMOWY</b>
                                                    </li>
                                                    <li>W przypadku odstąpienia od zawartej umowy Sprzedawca zwraca
                                                        Konsumentowi wszystkie otrzymane od niego płatności, w tym koszty
                                                        dostarczenia towaru (z wyjątkiem dodatkowych kosztów wynikających z
                                                        wybranego przez Konsumenta sposobu dostarczenia innego niż najtańszy
                                                        zwykły sposób dostarczenia oferowany przez Sprzedawcę),
                                                        niezwłocznie, a w każdym przypadku nie później niż 14 dni od dnia, w
                                                        którym Sprzedawca został poinformowany o decyzji Konsumenta o
                                                        wykonaniu prawa odstąpienia od umowy.
                                                    </li>
                                                    <li>Zwrotu płatności Sprzedawca dokona przy użyciu takich samych
                                                        sposobów płatności, jakie zostały przez Konsumenta użyte w
                                                        pierwotnej transakcji, chyba że Konsument zgodzi się na inne
                                                        rozwiązanie, w każdym przypadku Konsument nie poniesie żadnych opłat
                                                        w związku z tym zwrotem.
                                                    </li>
                                                    <li>Sprzedawca może wstrzymać się ze zwrotem płatności do czasu
                                                        otrzymania towaru lub do czasu dostarczenia mu dowodu jego
                                                        odesłania, w zależności od tego, które zdarzenie nastąpi wcześniej.
                                                    </li>
                                                    <li>
                                                        Sprzedawca prosi o zwracanie towaru na adres:
                                                        Ml sp. z o.o Głobino 74 76-200 Słupsk niezwłocznie, a w każdym razie
                                                        nie później niż 14 dni od dnia, w którym Konsument poinformował
                                                        Sprzedawcę o odstąpieniu od umowy sprzedaży. Termin jest zachowany,
                                                        jeżeli Konsument odeśle towar przed upływem terminu 14 dni.
                                                    </li>
                                                    <li>Konsument ponosi bezpośrednie koszty zwrotu towaru.</li>
                                                    <li>Konsument odpowiada tylko za zmniejszenie wartości towaru wynikające
                                                        z korzystania z niego w sposób inny niż było to konieczne do
                                                        stwierdzenia charakteru, cech i funkcjonowania towaru.
                                                    </li>
                                                    <li>Jeśli towar ze względu na swój charakter nie może zostać odesłany w
                                                        zwykłym trybie pocztą Konsument również będzie musiał ponieść
                                                        bezpośrednie koszty zwrotu towarów. O szacowanej wysokości tych
                                                        kosztów Konsument zostanie poinformowany przez Sprzedawcę w opisie
                                                        towaru w Sklepie lub podczas składania zamówienia.
                                                    </li>
                                                </ol>
                                                <h2>§ 8 WYJĄTKI OD PRAWA ODSTĄPIENIA OD UMOWY</h2>

                                                <ol>
                                                    <li>
                                                        Prawo odstąpienia od umowy zawartej na odległość nie przysługuje
                                                        Konsumentowi w odniesieniu do umowy:
                                                        <ol>
                                                            <li>W której przedmiotem świadczenia jest rzecz
                                                                nieprefabrykowana, wyprodukowana według specyfikacji
                                                                Konsumenta lub służąca zaspokojeniu jego
                                                                zindywidualizowanych potrzeb.
                                                            </li>
                                                            <li>W której przedmiotem świadczenia jest rzecz ulegająca
                                                                szybkiemu zepsuciu lub mająca krótki termin przydatności do
                                                                użycia.
                                                            </li>
                                                            <li>W której przedmiotem świadczenia jest rzecz dostarczana w
                                                                zapieczętowanym opakowaniu, której po otwarciu opakowania
                                                                nie można zwrócić ze względu na ochronę zdrowia lub ze
                                                                względów higienicznych, jeżeli opakowanie zostało otwarte po
                                                                dostarczeniu.
                                                            </li>
                                                            <li>W której przedmiotem świadczenia są rzeczy, które po
                                                                dostarczeniu, ze względu na swój charakter, zostają
                                                                nierozłącznie połączone z innymi rzeczami.
                                                            </li>
                                                            <li>W której przedmiotem świadczenia są nagrania dźwiękowe lub
                                                                wizualne albo programy komputerowe dostarczane w
                                                                zapieczętowanym opakowaniu, jeżeli opakowanie zostało
                                                                otwarte po dostarczeniu.
                                                            </li>
                                                            <li>O dostarczanie dzienników, periodyków lub czasopism, z
                                                                wyjątkiem umowy o prenumeratę.
                                                            </li>
                                                            <li>W której cena lub wynagrodzenie zależy od wahań na rynku
                                                                finansowym, nad którymi przedsiębiorca nie sprawuje
                                                                kontroli, i które mogą wystąpić przed upływem terminu do
                                                                odstąpienia od umowy.
                                                            </li>
                                                            <li>O dostarczanie treści cyfrowych, które nie są zapisane na
                                                                nośniku materialnym, jeżeli spełnianie świadczenia
                                                                rozpoczęło się za wyraźną zgodą Konsumenta przed upływem
                                                                terminu do odstąpienia od umowy i po poinformowaniu go przez
                                                                przedsiębiorcę o utracie prawa odstąpienia od umowy.
                                                            </li>
                                                        </ol></li>
                                                    <li>Prawo odstąpienia od umowy zawartej na odległość nie przysługuje
                                                        podmiotowi innemu niż Konsument.
                                                    </li>
                                                </ol>
                                                <h2>§ 9 REKLAMACJE</h2>

                                                <ol>
                                                    <li>W przypadku wystąpienia wady towaru Kupujący ma możliwość
                                                        reklamowania wadliwego towaru na podstawie uregulowanej w Kodeksie
                                                        cywilnym rękojmi lub gwarancji, o ile gwarancja została udzielona.
                                                    </li>
                                                    <li>
                                                        Korzystając z rękojmi Kupujący może, na zasadach oraz w terminach
                                                        określonych w Kodeksie cywilnym:
                                                        <ol>
                                                            <li>Złożyć oświadczenie o obniżeniu ceny</li>
                                                            <li>Przy wadzie istotnej - złożyć oświadczenie o odstąpieniu od
                                                                umowy
                                                            </li>
                                                            <li>Żądać wymiany rzeczy na wolną od wad</li>
                                                            <li>Żądać usunięcia wady</li>
                                                        </ol></li>
                                                    <li>Sprzedawca prosi o składanie reklamacji na podstawie rękojmi na
                                                        adres pocztowy lub elektroniczny wskazany w § 2 Regulaminu.
                                                    </li>
                                                    <li>
                                                        Jeśli okaże się, że dla rozpatrzenia reklamacji konieczne jest
                                                        dostarczenie reklamowanego towaru do Sprzedawcy, Kupujący jest
                                                        zobowiązany do dostarczenia tego towaru, w przypadku Konsumenta na
                                                        koszt Sprzedawcy, na adres
                                                        Ml sp. z o.o Głobino 74 76-200 Słupsk.
                                                    </li>
                                                    <li>Jeśli na towar została udzielona dodatkowo gwarancja, informacja o
                                                        niej, a także o jej warunkach, jest dostępna w opisie produktu w
                                                        Sklepie.
                                                    </li>
                                                    <li>Reklamacje dotyczące działania Sklepu należy kierować na adres
                                                        e-mail wskazany w § 2 Regulaminu.
                                                    </li>
                                                    <li>
                                                        Rozpatrzenie reklamacji przez Sprzedawcę nastąpi w terminie do 14
                                                        dni.
                                                        <br/><br/><b>POZASĄDOWE SPOSOBY ROZPATRYWANIA REKLAMACJI I DOCHODZENIA
                                                            ROSZCZEŃ</b>
                                                    </li>
                                                    <li>
                                                        W przypadku, gdy postępowanie reklamacyjne nie przyniesie
                                                        oczekiwanego przez Konsumenta rezultatu, Konsument może skorzystać
                                                        m.in. z:
                                                        <ol>
                                                            <li>Mediacji prowadzonej przez właściwy terenowo Wojewódzki
                                                                Inspektorat Inspekcji Handlowej, do którego należy się
                                                                zwrócić z wnioskiem o mediację. Co do zasady postępowanie
                                                                jest bezpłatne. Wykaz Inspektoratów znajduje się tutaj: <a
                                                                    href="https://www.uokik.gov.pl/wazne_adresy.php#faq595"
                                                                    rel="noopener noreferrer"
                                                                    target="_blank">https://www.uokik.gov.pl/wazne_adresy.php#faq595</a>.
                                                            </li>
                                                            <li>Pomocy właściwego terenowo stałego polubownego sądu
                                                                konsumenckiego działającego przy Wojewódzkim Inspektoracie
                                                                Inspekcji Handlowej, do którego należy złożyć wniosek o
                                                                rozpatrzenie sprawy przed sądem polubownym. Co do zasady
                                                                postępowanie jest bezpłatne. Wykaz sądów dostępny jest pod
                                                                adresem: <a
                                                                    href="https://www.uokik.gov.pl/wazne_adresy.php#faq596"
                                                                    rel="noopener noreferrer"
                                                                    target="_blank">https://www.uokik.gov.pl/wazne_adresy.php#faq596</a>.
                                                            </li>
                                                            <li>Bezpłatnej pomocy miejskiego lub powiatowego rzecznika
                                                                Konsumentów.
                                                            </li>
                                                            <li>Internetowej platformy ODR dostępnej pod adresem: <a
                                                                href="http://ec.europa.eu/consumers/odr/" rel="noopener noreferrer"
                                                                target="_blank">http://ec.europa.eu/consumers/odr/</a>.
                                                            </li>
                                                        </ol></li>
                                                </ol>
                                                <h2>§ 10 DANE OSOBOWE</h2>

                                                <ol>
                                                    <li>
                                                        Administratorem danych osobowych przekazanych przez Kupującego
                                                        podczas korzystania ze Sklepu jest Sprzedawca.
                                                    </li>
                                                    <li>
                                                        Dane osobowe Kupującego przetwarzane są na podstawie umowy i w celu
                                                        jej realizacji, zgodnie z zasadami określonymi w ogólnym
                                                        rozporządzeniu Parlamentu Europejskiego i Rady (EU) o ochronie
                                                        danych (RODO). Szczegółowe informacje dotyczące przetwarzania danych
                                                        przez Sprzedawcę zawiera polityka prywatności zamieszczona w
                                                        Sklepie.
                                                    </li>
                                                </ol>
                                                <h2>§ 11 ZASTRZEŻENIA</h2>

                                                <ol>
                                                    <li>Zakazane jest dostarczanie przez Kupującego treści o charakterze
                                                        bezprawnym.
                                                    </li>
                                                    <li>Każdorazowo składane w Sklepie zamówienie stanowi odrębną umowę
                                                        sprzedaży i wymaga osobnej akceptacji regulaminu. Umowa zawierana
                                                        jest na czas i w celu realizacji zamówienia.
                                                    </li>
                                                    <li>Umowy zawierane na podstawie niniejszego regulaminu zawierane są w
                                                        języku polskim.
                                                    </li>
                                                    <li>W przypadku ewentualnego sporu z Kupującym niebędącym Konsumentem
                                                        sądem właściwym będzie sąd właściwy dla siedziby Sprzedawcy.
                                                    </li>
                                                    <li>Żadne z postanowień niniejszego regulaminu nie wyłącza lub w żadnym
                                                        stopniu nie ogranicza uprawnień Konsumenta wynikających z przepisów
                                                        prawa.
                                                    </li>
                                                </ol>
                                                <br/><br/><b>Załącznik nr 1 do Regulaminu</b><br/><br/>

                                                    Poniżej znajduje się wzór formularza odstąpienia od umowy, z którego
                                                    Konsument może, ale nie musi skorzystać:

                                                    <br/><br/><br/><br/><p>
                                                        <strong>WZÓR FORMULARZA ODSTĄPIENIA OD UMOWY</strong><br/>
                                                        (formularz ten należy wypełnić i odesłać tylko w przypadku chęci
                                                        odstąpienia od umowy)
                                                    </p>

                                                        <p>
                                                            ML SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ<br/>
                                                            ul. Marszałka Józefa Piłsudskiego 30, 75-511 Koszalin<br/>
                                                            adres e-mail: shop@tuger.eu<br/></p>

                                                        <p>
                                                            - Ja
                                                            .........................................................................
                                                            niniejszym informuję o moim odstąpieniu od umowy sprzedaży
                                                            następujących rzeczy:<br/><br/>
                                                            ....................................................................................................................................................................................................................................<br/><br/>
                                                            ....................................................................................................................................................................................................................................<br/><br/>
                                                            ....................................................................................................................................................................................................................................<br/><br/>
                                                        </p>
                                                        <p>
                                                            - Data odbioru
                                                            .........................................................................................................................................................................................................<br/><br/>
                                                            - Imię i nazwisko Konsumenta(ów)
                                                            .....................................................................................................................................................................<br/><br/>
                                                            - Adres Konsumenta(ów)
                                                            ......................................................................................................................................................................................<br/><br/>
                                                            ....................................................................................................................................................................................................................................<br/><br/>
                                                        </p>
                                                        <p>
                                                            .............................................................................................<br/>
                                                            Podpis Konsumenta<br/>
                                                            <small>(tylko jeżeli formularz jest przesyłany w wersji
                                                                papierowej)
                                                            </small>
                                                            <br/><br/><br/>
                                                                Data ............................................
                                                        </p>
                                                        <p>
                                                            <small>(*) Niepotrzebne skreślić.</small>
                                                        </p>

                                                        <h1 className="center clearfix">Regulamin konta</h1>

                                                        <p className="center">
                                                            Regulamin konta w sklepie Tuger - haki samochodowe</p>

                                                        <p>
                                                            <b>SPIS TREŚCI</b><br/><b>§ 1</b> Definicje<br/><b>§ 2</b> Kontakt
                                                            ze Sprzedawcą<br/><b>§ 3</b> Wymogi techniczne<br/><b>§
                                                                4</b> Konto<br/><b>§ 5</b> Reklamacje<br/><b>§ 6</b> Dane
                                                                osobowe <br/><b>§ 7</b> Zastrzeżenia
                                                        </p>

                                                        <h2>§ 1 DEFINICJE</h2>

                                                        <b>Konto</b> – uregulowana w niniejszym regulaminie nieodpłatna
                                                        funkcja Sklepu (usługa), dzięki której Kupujący może założyć w
                                                        Sklepie swoje indywidualne Konto.<br/><b>Kupujący</b> - każdy podmiot
                                                            Kupujący w Sklepie. <br/><b>Sklep</b> – sklep internetowy Tuger -
                                                                haki samochodowe prowadzony przez Sprzedawcę pod adresem <a
                                                                    href="https://tuger.pl" rel="noopener noreferrer"
                                                                    target="_blank">https://tuger.pl</a><br/><b>Sprzedawca</b> -
                                                                    ML SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą
                                                                    pod adresem ul. Marszałka Józefa Piłsudskiego 30, 75-511
                                                                    Koszalin,
                                                                    wpisaną do Krajowego Rejestru Sądowego - rejestru
                                                                    przedsiębiorców przez SĄD REJONOWY W KOSZALINIE, IX
                                                                    WYDZIAŁ GOSPODARCZY KRAJOWEGO REJESTRU SĄDOWEGO, pod nr
                                                                    KRS 0000519814,
                                                                    NIP 6692522107, nr REGON 32155789500000, kapitał
                                                                    zakładowy 5000,00 zł.

                                                                    <br/><h2>§ 2 KONTAKT ZE SPRZEDAWCĄ</h2>

                                                                        <ol>
                                                                            <li>
                                                                                Adres pocztowy:
                                                                                ul. Marszałka Józefa Piłsudskiego 30, 75-511
                                                                                Koszalin
                                                                            </li>
                                                                            <li>
                                                                                Adres e-mail: shop@tuger.eu
                                                                            </li>
                                                                            <li>
                                                                                Telefon: 792001245
                                                                            </li>
                                                                        </ol>
                                                                        <h2>§ 3 WYMOGI TECHNICZNE</h2>

                                                                        <ol>
                                                                            <li>
                                                                                Dla prawidłowego funkcjonowania i założenia
                                                                                Konta potrzebne jest:
                                                                                <ul>
                                                                                    <li>Aktywne konto e-mail</li>
                                                                                    <li>Urządzenie z dostępem do Internetu
                                                                                    </li>
                                                                                    <li>Przeglądarka internetowa obsługująca
                                                                                        JavaScript i pliki cookies
                                                                                    </li>
                                                                                </ul></li>
                                                                        </ol>
                                                                        <h2>§ 4 KONTO</h2>

                                                                        <ol>
                                                                            <li>
                                                                                Założenie Konta jest całkowicie dobrowolne i
                                                                                zależne od woli Kupującego.
                                                                            </li>
                                                                            <li>Konto daje Kupującemu dodatkowe możliwości,
                                                                                takie jak: przeglądanie historii zamówień
                                                                                złożonych przez Kupującego w Sklepie,
                                                                                sprawdzenie statusu zamówienia czy
                                                                                samodzielna edycja danych Kupującego.
                                                                            </li>
                                                                            <li>W celu założenia Konta należy wypełnić
                                                                                stosowny formularz w Sklepie.
                                                                            </li>
                                                                            <li>W momencie założenia Konta zawierana jest na
                                                                                czas nieokreślony pomiędzy Kupującym a
                                                                                Sprzedawcą umowa w zakresie prowadzenia
                                                                                Konta na zasadach wskazanych w Regulaminie.
                                                                            </li>
                                                                            <li>Kupujący może bez ponoszenia jakichkolwiek
                                                                                kosztów w każdym czasie zrezygnować z Konta.
                                                                            </li>
                                                                            <li>W celu dokonania rezygnacji z Konta należy
                                                                                wysłać swoją rezygnację do Sprzedawcy na
                                                                                adres e-mail: shop@tuger.eu, czego skutkiem
                                                                                będzie niezwłoczne usunięcie Konta oraz
                                                                                rozwiązanie umowy w zakresie prowadzenia
                                                                                Konta.
                                                                            </li>
                                                                        </ol>
                                                                        <h2>§ 5 REKLAMACJE</h2>

                                                                        <ol>
                                                                            <li>Reklamacje dotyczące funkcjonowania Konta
                                                                                należy kierować na adres e-mail
                                                                                shop@tuger.eu.
                                                                            </li>
                                                                            <li>
                                                                                Rozpatrzenie reklamacji przez Sprzedawcę
                                                                                nastąpi w terminie do 14 dni.
                                                                                <br/><br/><b>POZASĄDOWE SPOSOBY ROZPATRYWANIA
                                                                                    REKLAMACJI I DOCHODZENIA ROSZCZEŃ</b>
                                                                            </li>
                                                                            <li>
                                                                                W przypadku gdy postępowanie reklamacyjne
                                                                                nie przyniesie oczekiwanego przez Konsumenta
                                                                                rezultatu Konsument może skorzystać m.in. z:
                                                                                <ol>
                                                                                    <li>Mediacji prowadzonej przez właściwy
                                                                                        terenowo Wojewódzki Inspektorat
                                                                                        Inspekcji Handlowej, do którego
                                                                                        należy się zwrócić z wnioskiem o
                                                                                        mediację. Co do zasady postępowanie
                                                                                        jest bezpłatne. Wykaz Inspektoratów
                                                                                        znajduje się tutaj: <a
                                                                                            href="https://www.uokik.gov.pl/wazne_adresy.php#faq595"
                                                                                            rel="noopener noreferrer"
                                                                                            target="_blank">https://www.uokik.gov.pl/wazne_adresy.php#faq595</a>.
                                                                                    </li>
                                                                                    <li>Pomocy właściwego terenowo stałego
                                                                                        polubownego sądu konsumenckiego
                                                                                        działającego przy Wojewódzkim
                                                                                        Inspektoracie Inspekcji Handlowej,
                                                                                        do którego należy złożyć wniosek o
                                                                                        rozpatrzenie sprawy przed sądem
                                                                                        polubownym. Co do zasady
                                                                                        postępowanie jest bezpłatne. Wykaz
                                                                                        sądów dostępny jest pod adresem: <a
                                                                                            href="https://www.uokik.gov.pl/wazne_adresy.php#faq596"
                                                                                            rel="noopener noreferrer"
                                                                                            target="_blank">https://www.uokik.gov.pl/wazne_adresy.php#faq596</a>.
                                                                                    </li>
                                                                                    <li>Internetowej platformy ODR dostępnej
                                                                                        pod adresem: <a
                                                                                            href="http://ec.europa.eu/consumers/odr/"
                                                                                            rel="noopener noreferrer"
                                                                                            target="_blank">http://ec.europa.eu/consumers/odr/</a>.
                                                                                    </li>
                                                                                </ol></li>
                                                                        </ol>
                                                                        <h2>§ 6 DANE OSOBOWE</h2>

                                                                        <ol>
                                                                            <li>
                                                                                Administratorem danych osobowych
                                                                                przekazanych przez Kupującego podczas
                                                                                korzystania ze Sklepu jest Sprzedawca.
                                                                            </li>
                                                                            <li>
                                                                                Dane osobowe Kupującego przetwarzane są na
                                                                                podstawie umowy i w celu jej realizacji,
                                                                                zgodnie z zasadami określonymi w ogólnym
                                                                                rozporządzeniu Parlamentu Europejskiego i
                                                                                Rady (EU) o ochronie danych (RODO).
                                                                                Szczegółowe informacje dotyczące
                                                                                przetwarzania danych przez Sprzedawcę
                                                                                zawiera polityka prywatności zamieszczona w
                                                                                Sklepie.
                                                                            </li>
                                                                        </ol>
                                                                        <h2>§ 7 ZASTRZEŻENIA</h2>

                                                                        <ol>
                                                                            <li>Zakazane jest dostarczanie przez Kupującego
                                                                                treści o charakterze bezprawnym.
                                                                            </li>
                                                                            <li>Umowa w zakresie prowadzenia Konta zawierana
                                                                                jest w języku polskim.
                                                                            </li>
                                                                            <li>W przypadku zaistnienia ważnych przyczyn, o
                                                                                których mowa w ust. 4, Sprzedawca ma prawo
                                                                                do zmiany niniejszego regulaminu Konta.
                                                                            </li>
                                                                            <li>
                                                                                Ważnymi przyczynami, o których mowa w ust. 3
                                                                                są:
                                                                                <ol>
                                                                                    <li>konieczność dostosowania Sklepu do
                                                                                        przepisów prawa mających
                                                                                        zastosowanie do działalności Sklepu
                                                                                    </li>
                                                                                    <li>poprawa bezpieczeństwa świadczonej
                                                                                        usługi
                                                                                    </li>
                                                                                    <li>zmiana funkcjonalności Konta
                                                                                        wymagająca modyfikacji regulaminu
                                                                                        Konta.
                                                                                    </li>
                                                                                </ol></li>
                                                                            <li>Kupujący zostanie poinformowany o planowanej
                                                                                zmianie regulaminu Konta co najmniej na 7
                                                                                dni przed wprowadzeniem zmiany w życie za
                                                                                pośrednictwem wiadomości e-mail wysłanej na
                                                                                przypisany do Konta adres.
                                                                            </li>
                                                                            <li>W przypadku gdy Kupujący nie wyrazi
                                                                                akceptacji dla planowanej zmiany, powinien
                                                                                poinformować o tym Sprzedawcę poprzez
                                                                                wysłanie odpowiedniej wiadomości na adres
                                                                                e-mail Sprzedawcy shop@tuger.eu, czego
                                                                                skutkiem będzie rozwiązanie umowy w zakresie
                                                                                prowadzenia Konta z chwilą wejścia w życie
                                                                                planowanej zmiany lub wcześniej, jeśli
                                                                                Kupujący zgłosi takie żądanie.
                                                                            </li>
                                                                            <li>W sytuacji gdy Kupujący nie wyrazi sprzeciwu
                                                                                dla planowanej zmiany do chwili wejścia jej
                                                                                w życie przyjmuje się, że akceptuje ją, co
                                                                                nie stanowi żadnej przeszkody do rozwiązania
                                                                                umowy w przyszłości.
                                                                            </li>
                                                                            <li>W przypadku ewentualnego sporu z Kupującym
                                                                                nie będącym Konsumentem sądem właściwym
                                                                                będzie sąd właściwy dla siedziby Sprzedawcy.
                                                                            </li>
                                                                            <li>Żadne z postanowień niniejszego regulaminu
                                                                                nie wyłącza lub w żadnym stopniu nie
                                                                                ogranicza uprawnień Konsumenta wynikających
                                                                                z przepisów prawa.
                                                                            </li>
                                                                        </ol></div>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default terms;