import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Breadcrumbs} from "../../shared/Breadcrumbs";
import { Link } from "react-router-dom";
import {doChangeOrderAttribute} from "../../../actions_creators/order";
import {countryOptions} from "../../../constants/orderOptions";
import {ProductsList} from "../shared/ProductsList";
import {formatPriceToString, sumDeliveriesInBasket, sumProductsInBasket} from "../../../helper_functions/priceHelpers";

class OrderAddress extends Component {

    paramsForBreadcrumbs = () => {
        return [{name: 'Koszyk', path: '/basket'}]
    };

    changeOrderAttribute = (attribute, event) => {
        let obj = {};
        obj[attribute] = event.target.value;
        this.props.onOrderAttributeChange(obj);
    };

    billingFromShipping = () => {
        const {delivery_name, delivery_surname, delivery_phone, delivery_address,
            delivery_post_code, delivery_country, delivery_city} = this.props.orderState;

        this.props.onOrderAttributeChange({billing_name: delivery_name});
        this.props.onOrderAttributeChange({billing_surname: delivery_surname});
        this.props.onOrderAttributeChange({billing_address: delivery_address});
        this.props.onOrderAttributeChange({billing_city: delivery_city});
        this.props.onOrderAttributeChange({billing_post_code: delivery_post_code});
        this.props.onOrderAttributeChange({billing_country: delivery_country});
        this.props.onOrderAttributeChange({billing_phone: delivery_phone});
    };
    // development/staging only
    fillWithSampleData = () => {
      this.props.onOrderAttributeChange({delivery_name: 'Jan'});
      this.props.onOrderAttributeChange({delivery_surname: 'Kowalski'});
      this.props.onOrderAttributeChange({email: 'mrJan@example.com'});
      this.props.onOrderAttributeChange({delivery_phone: '+0 0000 0000'});
      this.props.onOrderAttributeChange({delivery_address: 'Sewers'});
      this.props.onOrderAttributeChange({delivery_city: 'Neverwhere'});
      this.props.onOrderAttributeChange({delivery_post_code: '77-300'});
    };

    isValidOrder = () => {
        const {email, billing_name, billing_surname, billing_phone, billing_address, billing_post_code,
            delivery_name, delivery_surname, delivery_phone, delivery_address, billing_city,
            delivery_post_code, delivery_city} = this.props.orderState;
        return (email.length > 2 && billing_name.length > 2 && billing_surname.length > 2 && billing_phone.length > 2 &&
        billing_address.length > 2 && billing_post_code.length > 2 && delivery_name.length > 2 && delivery_surname.length > 2 &&
        delivery_phone.length > 2 && delivery_address.length > 2 && billing_city.length > 2 && delivery_post_code.length > 2 &&
        delivery_city.length > 2 && this.props.inBasket.length > 0);
    };

    render() {
        const {email, billing_name, billing_surname, billing_phone, billing_address, billing_post_code, comment,
            billing_country, delivery_name, delivery_surname, delivery_phone, delivery_address, billing_city, tax_number,
            delivery_post_code, delivery_country, delivery_option, delivery_city} = this.props.orderState;
        const deliverySum = sumDeliveriesInBasket(this.props.inBasket, delivery_option);
        return (
            <React.Fragment>
                <Breadcrumbs name='Koszyk' urls={this.paramsForBreadcrumbs()}/>
                <section id="content" className="address">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-md-12">
                                <p>{process.env.NODE_ENV === 'development' &&
                                <button className="ui button" onClick={this.fillWithSampleData}>Fill With Sample Client</button>
                                }</p>
                                <div className="row">
                                    <div className="col-lg-12">
                                    <h4>Dane adresowe</h4>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Imię</span>
                                        <input type="text" placeholder="Wpisz imię" value={delivery_name} onChange={(ev) => this.changeOrderAttribute('delivery_name', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Nazwisko</span>
                                        <input type="text" placeholder="Wpisz nazwisko" value={delivery_surname} onChange={(ev) => this.changeOrderAttribute('delivery_surname', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Adres e-mail</span>
                                        <input type="text" placeholder="Wpisz e-mail" value={email} onChange={(ev) => this.changeOrderAttribute('email', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Telefon</span>
                                        <input type="text" placeholder="Wpisz numer telefonu" value={delivery_phone} onChange={(ev) => this.changeOrderAttribute('delivery_phone', ev)}/>
                                    </div>
                                    <div className="col-lg-4">
                                        <span>Ulica i numer domu/mieszkania</span>
                                        <input type="text" placeholder="Wpisz ulicę i numer domu" value={delivery_address} onChange={(ev) => this.changeOrderAttribute('delivery_address', ev)}/>
                                    </div>
                                    <div className="col-lg-4">
                                        <span>Miasto</span>
                                        <input type="text" placeholder="Wpisz miasto" value={delivery_city} onChange={(ev) => this.changeOrderAttribute('delivery_city', ev)}/>
                                    </div>
                                    <div className="col-lg-4">
                                        <span>Kod pocztowy</span>
                                        <input type="text" placeholder="Wpisz kod pocztowy" value={delivery_post_code} onChange={(ev) => this.changeOrderAttribute('delivery_post_code', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Kraj</span>
                                        <div className="styled-select">
                                            <select onChange={(ev) => this.changeOrderAttribute('delivery_country', ev)}>
                                                { countryOptions.map(item => (
                                                    <option value={item.value} selected={delivery_country === item.value} key={item.value}>{item.name}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="space"/>
                                    <div className="col-lg-12">
                                        <h4>Dane do faktury</h4>
                                    </div>
                                    <div className="discount-btn" onClick={this.billingFromShipping}>Skopiuj z danych adresowych</div>
                                    <div className="col-lg-6">
                                        <span>Imię lub nazwa firmy</span>
                                        <input type="text" placeholder="Wpisz imię" value={billing_name} onChange={(ev) => this.changeOrderAttribute('billing_name', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Nazwisko / Nazwa firmy c.d.</span>
                                        <input type="text" placeholder="Wpisz imię" value={billing_surname} onChange={(ev) => this.changeOrderAttribute('billing_surname', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>NIP</span>
                                        <input type="text" placeholder="Wpisz NIP (o ile dotyczy)" value={tax_number} onChange={(ev) => this.changeOrderAttribute('tax_number', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Telefon</span>
                                        <input type="text" placeholder="Wpisz telefon" value={billing_phone} onChange={(ev) => this.changeOrderAttribute('billing_phone', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Ulica i numer domu/mieszkania</span>
                                        <input type="text" placeholder="Wpisz ulicę i numer domu/mieszkania" value={billing_address} onChange={(ev) => this.changeOrderAttribute('billing_address', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Miasto</span>
                                        <input type="text" placeholder="Wpisz miasto" value={billing_city} onChange={(ev) => this.changeOrderAttribute('billing_city', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Kod pocztowy</span>
                                        <input type="text" placeholder="Wpisz kod pocztowy" value={billing_post_code} onChange={(ev) => this.changeOrderAttribute('billing_post_code', ev)}/>
                                    </div>
                                    <div className="col-lg-6">
                                        <span>Kraj</span>
                                        <div className="styled-select">
                                            <select onChange={(ev) => this.changeOrderAttribute('billing_country', ev)}>
                                                { countryOptions.map(item => (
                                                    <option value={item.value} selected={billing_country === item.value} key={item.value}>{item.name}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="space"/>
                                    <div className="col-lg-12">
                                        <h4>Informacje dodatkowe</h4>
                                    </div>
                                    <textarea placeholder="wpisz uwagi..." value={comment} onChange={(ev) => this.changeOrderAttribute('comment', ev)}/>
                                </div>
                            </div>
                            <ProductsList basket={this.props.inBasket}>
                                <div className="sub-total">
                                    <label>Łącznie do zapłaty</label>
                                    <span>{formatPriceToString(sumProductsInBasket(this.props.inBasket, deliverySum))}</span>
                                </div>

                                <hr/>
                                {this.isValidOrder() ? <Link to='order-summary'><div className="button">Przejdź do podsumowania</div></Link> : <div className="button disabled-search-button">Wypełnij pola adresowe by przejść do podsumowania</div>}

                            </ProductsList>
                            <div className="col-lg-12">
                                <div className="space"/>
                                <Link to="/products"><span className="continue"><i className="fas fa-chevron-left"/>Kontynuuj zakupy</span></Link>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onOrderAttributeChange: (attribute) => dispatch(doChangeOrderAttribute(attribute)),
    };
}

function mapStateToProps(state) {
    return {
        inBasket: state.basketState.inBasket,
        orderState: state.orderState
    };
}

const ConnectedOrder = connect(mapStateToProps, mapDispatchToProps)(OrderAddress);


export default ConnectedOrder;