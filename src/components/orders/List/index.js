import React, {Component} from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";
import fetchResourceFromBackEnd from "../../../helper_functions/fetcher";
import {OrderTable} from "../shared/OrderTable";
import {connect} from "react-redux";
import {
    Link
} from 'react-router-dom';


class OrdersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchingError: undefined,
            orders: [],
        }
    }

    loggedIn = () => {
        const {token, expires_at} = this.props.userSession;
        return (token !== null && new Date(expires_at) > new Date())
    };

    onFetchSuccess = (res) => {
        this.setState({orders: res, ordersFetched: true});
    };

    onFetchFail = (err) => {
        this.setState({fetchingError: err.message});
    };

    paramsForBreadcrumbs = () => {
        return [{name: 'Lista zamówień', path: '/orders'}]
    };

    componentDidMount() {
        if (!this.loggedIn()) {return;}
        fetchResourceFromBackEnd('orders', this.onFetchSuccess, this.onFetchFail);
    };

    render() {
        const {fetchingError, orders} = this.state;
        if (!this.loggedIn() || fetchingError !== undefined) {
            return (
              <React.Fragment>
                <Breadcrumbs urls={this.paramsForBreadcrumbs()} name='Lista zamówień'/>
                  <section id="content" className="account">
                      <div className="container">
                          <div className="row">
                              <div className="col-lg-12">
                                  <h4>Lista zamówień</h4>
                                  <div className="history-box">
                                      {fetchingError !== undefined ?
                                          <strong>Wystąpił błąd: {fetchingError}</strong>
                                          :
                                          <strong>Musisz <Link to='login'>być zalogowany</Link> by móc obejrzeć tę
                                              stronę</strong>
                                      }
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
              </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <Breadcrumbs urls={this.paramsForBreadcrumbs()} name='Lista zamówień'/>
                <section id="content" className="account">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <h4>Lista zamówień</h4>
                                <div className="history-box">
                                    <OrderTable orders={orders}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        userSession: state.userSessionState,
    }
}


const ConnectedOrdersList = connect(mapStateToProps, null)(OrdersList);

export default ConnectedOrdersList;