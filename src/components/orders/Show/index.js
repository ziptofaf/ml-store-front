import React, {Component} from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";
import fetchResourceFromBackEnd from "../../../helper_functions/fetcher";
import {OrderTable} from "../shared/OrderTable";

class OrderShow extends Component {
    constructor(props){
        super(props);
        this.state = {
            orderFetched: false,
            fetchingError: undefined,
            order: undefined,
            orderId: this.props.match.params.id
        }
    }

    paramsForBreadcrumbs = () => {
        return [{name: 'Zamówienie', path: '/orders/' + this.state.orderId}]
    };

    onFetchFail = (err) => {
        this.setState({fetchingError: err.message});
    };

    onFetchSuccess = (res) => {
        if (res.status === 'error') {
            throw new Error(res.message);
        }
        this.setState({order: res, orderFetched: true})
    };

    render() {
        const {orderFetched, fetchingError, order} = this.state;
        return (
            <React.Fragment>
                <Breadcrumbs name='Podsumowanie zamówienia' urls={this.paramsForBreadcrumbs()}/>
                <section id="content" className="account">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <h4>Status zamówienia</h4>
                                <div className="history-box">
                                    {fetchingError !== undefined && <strong>{fetchingError}</strong>}
                                    {fetchingError === undefined && orderFetched && <OrderTable orders={[order]}/>}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }

    componentDidMount() {
        fetchResourceFromBackEnd('orders/' + this.state.orderId, this.onFetchSuccess, this.onFetchFail);
    }

}

export {OrderShow};