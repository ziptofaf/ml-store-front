import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Breadcrumbs} from "../../shared/Breadcrumbs";
import { Link } from "react-router-dom";
import {doChangeOrderAttribute} from "../../../actions_creators/order";
import {countryOptions, deliveryOptions} from "../../../constants/orderOptions";
import {ProductsList} from "../shared/ProductsList";
import {formatPriceToString, sumDeliveriesInBasket, sumProductsInBasket} from "../../../helper_functions/priceHelpers";
import {sendNewOrder} from "../actions/sendNewOrder";
import {doEmptyBasket} from "../../../actions_creators/basket";

class OrderSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            acceptedTos: false,
            orderCreationFailed: false,
            creationFailedMessage: '',
            orderCreated: false,
            orderPaymentUrl: null
        }
    }

    paramsForBreadcrumbs = () => {
        return [{name: 'Koszyk', path: '/basket'}, {name: 'Podsumowanie zamówienia', path: 'order-summary'}]
    };

    changeTos = () => {
        const newState = !this.state.acceptedTos;
        this.setState({acceptedTos: newState});
    };

    orderProducts = () => {
        sendNewOrder(this.props.orderState, this.props.inBasket, this.onOrderCreation, this.onOrderCreationFailed)
    };

    onOrderCreation = (res) => {
        if (res.status === 'ok') {
            this.setState({orderCreated: true, orderPaymentUrl: res.payment.url});
            this.props.onBasketEmpty();
        }
        else {
            throw new Error(res.message);
        }
    };

    onOrderCreationFailed = (err) => {
        this.setState({orderCreationFailed: true, creationFailedMessage: err.message})
    };

    selectedDeliveryCountry = () => {
      return countryOptions.find((item) => (item.value === this.props.orderState.delivery_country)).name
    };

    selectedBillingCountry = () => {
        return countryOptions.find((item) => (item.value === this.props.orderState.billing_country)).name;
    };

    selectedDeliveryMode = () => {
        return deliveryOptions.find((item) => (item.value === this.props.orderState.delivery_option)).name;
    };
    render() {
        const {billing_name, billing_surname, billing_phone, billing_address, billing_post_code,
               delivery_name, delivery_surname, delivery_phone, delivery_address, billing_city,
               delivery_post_code, delivery_option, delivery_city} = this.props.orderState;
        const {acceptedTos, orderCreationFailed, creationFailedMessage, orderCreated, orderPaymentUrl} = this.state;
        const deliverySum = sumDeliveriesInBasket(this.props.inBasket, delivery_option);
        if (orderCreated) {
            return (
              <React.Fragment>
                  <Breadcrumbs name='Podsumowanie zamówienia' urls={this.paramsForBreadcrumbs()}/>
                  <section id="content" className="address">
                      <div className="container">
                          <div className="row">
                              <div className="col-lg-12 text-center">
                                  {orderPaymentUrl === null ?
                                  <p>Dziękujemy za dokonanie zamówienia. Wybrałeś model płatności pobraniowej lub przelewem.
                                      W pierwszym przypadku nie musisz nic robić, potwierdzenie wysyłki wyślemy do ciebie emailem.<br/>
                                      Jeśli wybrałeś metodę płatności przelewem to ....
                                  </p> :
                                      <p><a href={orderPaymentUrl} className="btn btn-lg btn-info" target='_blank' rel='noopener noreferrer'>Kliknij aby opłacić zamówienie</a></p>}
                              </div>
                          </div>
                      </div>
                  </section>
              </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <Breadcrumbs name='Podsumowanie zamówienia' urls={this.paramsForBreadcrumbs()}/>
                <section id="content" className="address">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 order-2 order-lg-1 address-margin">
                                <div className="row">
                                    <div className="col-lg-4 col-md-6">
                                        <h4>Adres wysyłki</h4>
                                        <p>
                                            {delivery_name} {delivery_surname}<br/>
                                            {delivery_address}<br/>
                                            {delivery_post_code} {delivery_city}<br/>
                                            {this.selectedDeliveryCountry()}<br/>
                                            tel. {delivery_phone}<br/>
                                            <Link to='/new-order'><span className="change">(zmień)</span></Link>
                                        </p>
                                    </div>
                                    <div className="col-lg-4 col-md-6">
                                        <h4>Adres na fakturze</h4>
                                        <p>
                                            {billing_name} {billing_surname}<br/>
                                            {billing_address}<br/>
                                            {billing_post_code} {billing_city}<br/>
                                            {this.selectedBillingCountry()}<br/>
                                            tel. {billing_phone}<br/>
                                            <Link to='/new-order'><span className="change">(zmień)</span></Link>
                                        </p>
                                    </div>
                                </div>
                                <hr/>
                                <label className="checkbox">
                                    Zapoznałem się z <Link to="/terms" className="underline">regulaminem</Link> sklepu internetowego i akceptuję jego treść
                                    <input type="checkbox" checked={acceptedTos} onChange={this.changeTos}/>
                                    <span className="checkmark"/>
                                </label>
                                <button className="button" disabled={!acceptedTos} onClick={this.orderProducts}>Złóż zamówienie</button>
                                { orderCreationFailed &&
                                    <p><br/>Coś poszło nie tak: {creationFailedMessage}</p>
                                }
                            </div>
                            <ProductsList basket={this.props.inBasket} delivery={delivery_option} classNames='order-1 order-lg-2'>
                                <div className="sub-total">
                                    <label>Łącznie do zapłaty</label>
                                    <span>{formatPriceToString(sumProductsInBasket(this.props.inBasket, deliverySum))}</span>
                                </div>
                                <div className="sub-total">
                                    <label>Forma dostawy</label>
                                    <span>{this.selectedDeliveryMode()}</span>
                                </div>
                            </ProductsList>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onOrderAttributeChange: (attribute) => dispatch(doChangeOrderAttribute(attribute)),
        onBasketEmpty: () => dispatch(doEmptyBasket())
    };
}

function mapStateToProps(state) {
    return {
        inBasket: state.basketState.inBasket,
        orderState: state.orderState
    };
}

const ConnectedOrder = connect(mapStateToProps, mapDispatchToProps)(OrderSummary);


export default ConnectedOrder;