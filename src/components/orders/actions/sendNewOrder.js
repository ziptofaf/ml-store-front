import fetchResourceFromBackEnd from "../../../helper_functions/fetcher";

function sendNewOrder(orderState, basketState, sendingSuccessful, sendingFailed) {
    let orderItems = {order_items_attributes: prepareItemsFromBasket(basketState)};
    const params = {order: {...orderState, ...orderItems}};
    return fetchResourceFromBackEnd('orders.json', sendingSuccessful, sendingFailed, params, 'POST');
}


function prepareItemsFromBasket(basketState) {
    return basketState.map((item) =>  ({quantity: item.quantity, sellable_id: item.id, sellable_type: item.type}) )
}

export {sendNewOrder};