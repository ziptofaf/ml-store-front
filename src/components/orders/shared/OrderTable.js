import React from 'react';
import {formatPriceToString} from "../../../helper_functions/priceHelpers";
import {paymentOptions} from "../../../constants/orderOptions";
import {orderStates} from "../../../helper_functions/orderStates";

const parseDateToString = (date) =>
{
    return new Date(Date.parse(date)).toLocaleDateString('pl-PL');
};

const paymentSystemToString = (paymentSystem) => {
    return paymentOptions.find((item) => (item.value === paymentSystem)).name
};

const orderStateToString = (orderStatus) => {
    return orderStates.find((item) => (item.value === orderStatus)).name
};

const trackingUrl = (order) => {
    if (order.package_tracking_url === null) {
        return <span>Numer paczki jeszcze nie został dodany</span>
    }
    else {
        return <a href={order.package_tracking_url} target="_blank" rel="noopener noreferrer"className="btn btn-primary">Śledzenie paczki</a>
    }
};

const OrderTable = ({orders}) => (
  <table>
      <thead className="thead-default">
        <tr>
            <th>Data</th>
            <th>Kwota całkowita</th>
            <th>Rodzaj płatności</th>
            <th>Status</th>
            <th>Śledzenie</th>
        </tr>
      </thead>
      <tbody>
        {orders.map( (order) => (
        <tr key={order.id}>
            <td>{parseDateToString(order.created_at)}</td>
            <td>{formatPriceToString(parseFloat(order.price))}</td>
            <td>{paymentSystemToString(order.payment.payment_system)}</td>
            <td>{orderStateToString(order.state)}</td>
            <td>{trackingUrl(order)}</td>
        </tr>
        ))}
      </tbody>
  </table>
);

export {OrderTable}