import React from "react";
import MAIN_API_PATH from "../../../constants/urls";
import {
    formatPriceToString,
    priceWithQuantity,
} from "../../../helper_functions/priceHelpers";

const ProductsList = ({children, basket, classNames}) => {
    return (
    <div className={"col-lg-4 marginMobileTop" + classNames}>
        <ul className="cart-items">
            {basket.map((item) => (
                <li className="cart-item" key={item.id}>
                    <div className="product-line-grid">
                        <div className="row">
                            <div className="col-lg-3 col-md-3 col-3 product-photo">
                                <img className="img-fluid" src={MAIN_API_PATH+item.image_url} alt="zdjęcie haka"/>
                            </div>
                            <div className="col-lg-6 col-md-6 col-6 vertical-center">
                                <h4>{item.quantity} x {item.name}</h4>
                            </div>
                            <div className="col-lg-3 col-md-3 col-3 vertical-center">
                                <span className="price">{formatPriceToString(priceWithQuantity(item))}</span>
                            </div>
                        </div>
                    </div>
                </li>
            ))}
        </ul>
        {children}
    </div>
    );
};

export {ProductsList}