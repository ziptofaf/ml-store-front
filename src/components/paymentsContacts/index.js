export {default as Delivery} from './partials/Delivery';
export {default as Contact} from './partials/Contact';
export {default as PaymentMethods} from './partials/PaymentMethods';
