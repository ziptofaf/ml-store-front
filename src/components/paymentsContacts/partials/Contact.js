import React from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Kontakt i dane firmy', path: '/contact-us'}]
}

const contact = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Kontakt i dane firmy'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <span className='center document-heading center'>Adres</span><br />
                        <p>
                            <span>ML sp. z o.o.</span>
                            <span><br />Marszałka J&oacute;zefa Piłsudskiego 30<br /><br /></span>
                            <span>75-511 Koszalin<br />NIP: 6692522107 </span><strong>
                            <span><br /></span></strong>
                        </p>
                        <p>
                            <span>Kontakt telefoniczny (od poniedziałku do piątku, w godzinach 9:00-14:00): 792-00-12-45<br />Kontakt mailowy: shop@tuger.eu<br /></span>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default contact;