import React from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Zwroty i reklamacje', path: '/delivery'}]
}

const delivery = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Czas i koszty dostawy'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <p><strong>Koszt<br/></strong>
                            Przesyłka w naszym sklepie jest darmowa, niezależnie od wybranego rodzaju płatności.
                        </p>
                        <br/>
                        <p><strong>Czas dostawy<br/></strong>
                            Całkowity czas realizacji zamówienia (wraz z dostawą) wynosi średnio 1-2 dni roboczych. W szczególnych przypadkach ten okres może się wydłużyć - skontaktujemy się wówczas z tobą.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default delivery;