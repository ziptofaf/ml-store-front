import React from 'react';
import {Breadcrumbs} from "../../shared/Breadcrumbs";

function paramsForBreadcrumbs () {
    return [{name: 'Formy płatności', path: '/payment-methods'}]
}

const paymentMethods = () => (
    <React.Fragment>
        <Breadcrumbs urls={paramsForBreadcrumbs()} name='Formy płatności'/>
        <section id="content" className="page">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <p><strong>Za pobraniem<br/></strong>
                        Zapłata gotówką kurierowi przy odbiorze zamówienia.
                        </p>
                        <br/>
                        <p><strong>Przelewem online<br/></strong>
                        Forma płatności elektronicznej obsługiwana przez PayU
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
);

export default paymentMethods;