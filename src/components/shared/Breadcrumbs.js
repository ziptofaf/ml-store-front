import {Link} from "react-router-dom";
import React from "react";

const Breadcrumbs = ({name, urls}) => {
    let urlsWithHome = [{name: 'Strona główna', path: '/'}, ...urls];
    for (let i = 0; i<urlsWithHome.length - 1; i++) {
        urlsWithHome[i].name = urlsWithHome[i].name + ' > ';
    }
    return (
    <section id="breadcrumbs">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h2>{name}</h2>
                        <p>
                            {
                                urlsWithHome.map((url) => (
                                    <React.Fragment key={url.name}>
                                        <Link to={url.path} key={url.name}>{url.name}</Link>
                                    </React.Fragment>

                                ))
                            }
                        </p>
                </div>
            </div>
        </div>
    </section>
    );
};

export {Breadcrumbs};