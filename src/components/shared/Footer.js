import React from 'react';
import {
    Link
} from 'react-router-dom';

const Footer = () => (
    <React.Fragment>
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-5">
                        <div className="logo-footer">
                            <Link to='/'><img src="/img/logo.png" className="img-fluid" alt=""/></Link>
                        </div>
                        <p><span className="bold">Adres</span><br/>
                            ML sp. z o.o.<br/>
                            Marszałka Józefa Piłsudskiego 30<br/>
                            75-511 Koszalin<br/><br/>

                            <span className="bold">Potrzebujesz pomocy?</span><br/>
                            Zadzwoń: 792 001 245
                        </p>
                    </div>
                    <div className="col-lg-8 col-md-7">
                        <div className="row">
                            <div className="col-lg-8 col-md-6">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <h4>Informacje</h4>
                                        <ul>
                                            <li><Link to='/terms'>Regulamin</Link></li>
                                            <li><Link to='/returns'>Zwroty i reklamacje</Link></li>
                                            <li><Link to='/privacy'>Polityka prywatności</Link></li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-6">
                                        <h4>Moje konto</h4>
                                        <ul>
                                            <li><Link to='/orders'>Twoje zamówienia</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <h4>Kontakt i płatności</h4>
                                <ul>
                                    <li><Link to="/payment-methods">Formy płatności</Link></li>
                                    <li><Link to='/delivery'>Czas i koszty dostawy</Link></li>
                                    <li><Link to="/contact-us">Kontakt i dane firmy</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12">
                        <hr/>
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <p>Copyright @ Tuger - haki holownicze. All Rights Reserved</p>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="payments">
                                    <img src="/img/payments.jpg" className="img-fluid" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </React.Fragment>
);

export default Footer;