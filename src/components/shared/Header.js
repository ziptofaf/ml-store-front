import React, { Component } from 'react';

import {
    Link
} from 'react-router-dom';
import MiniBasket from "./MiniBasket";
import {doChangeUserSessionToken} from "../../actions_creators/userSession";
import {connect} from "react-redux";

class Header extends Component {

    shouldDisplayLogin = () => {
        const {token, expires_at} = this.props.userSession;
        return (token === null || new Date(expires_at) < new Date());
    };

    logOut = () => {
        this.props.onTokenChange(null);
    };
    render() {
        return (
            <React.Fragment>
                <section id="top">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <ul>
                                    <li><a href="mailto:shop@tuger.eu"><img src="/img/email.svg"
                                                                            className="email-icon" alt="email-us"/> shop@tuger.eu</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <ul className="toRight">
                                    {this.shouldDisplayLogin() ?
                                        <li><Link to='/login'>Zaloguj się</Link> | <Link to='/register'>Zarejestruj</Link></li>
                                        :
                                        <li onClick={this.logOut} className="clickable">Wyloguj się</li>
                                    }
                                    <li><img src="/img/pl-flag.jpg" alt=""/> Polski <img src="/img/arrow-down.png"
                                                                                         alt=""/></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <header>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 col-md-3">
                                <Link to='/'><img src="/img/logo.png" className="img-fluid" alt=""/></Link>
                            </div>
                            <div className="col-lg-8 col-md-9">
                                <ul>
                                    <li className="phone-call">
                                        <span>Telefon kontaktowy</span>
                                        <p>792-00-12-45</p>
                                    </li>
                                    <MiniBasket/>
                                </ul>
                            </div>
                        </div>
                    </div>
                </header>
            </React.Fragment>
        );
    }
}


function mapStateToProps(state) {
    return {
        userSession: state.userSessionState,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onTokenChange: (token) => dispatch(doChangeUserSessionToken(token))
    }
}

const ConnectedHeader= connect(mapStateToProps, mapDispatchToProps)(Header);


export default ConnectedHeader;