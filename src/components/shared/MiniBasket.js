import React, { Component } from 'react';

import {
    Link
} from 'react-router-dom';

import { connect } from 'react-redux';
import {sumProductsInBasket, formatPriceToString} from "../../helper_functions/priceHelpers";


class MiniBasket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            glowing: false,
            timer: undefined
        }
    }
    render() {
        const classNameForBasket = this.state.glowing ? 'num-basket basket-glow' : 'num-basket'
        return (
            <Link to='/basket'>
                <li className="basket">
                    <div className={classNameForBasket}>{this.props.inBasket.length}</div>
                    <span>Koszyk</span>
                    <p>{this.sumPrices()}</p>
                </li>
            </Link>
        );
    }

    sumPrices = () => {
        return formatPriceToString(sumProductsInBasket(this.props.inBasket));
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.inBasket !== this.props.inBasket) {
            this.setState({glowing: true});
            this.timer = setTimeout(() => {
                this.setState({glowing: false});
            }, 1000);
        }
    };

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

}

function mapStateToProps(state) {
    return {
        inBasket: state.basketState.inBasket,
    };
}


const ConnectedMiniBasket = connect(mapStateToProps, null)(MiniBasket);



export default ConnectedMiniBasket;