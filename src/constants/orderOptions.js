const paymentOptions = [
    {name: 'Pobranie', value: 'cash_on_delivery'},
    {name: 'Przelew', value: 'wired'},
    {name: 'PayU', value: 'payu'}
];

const deliveryOptions = [
    {name: 'Kurier DPD', value: 'dpd', price: 0.00}
];

const countryOptions = [
    {name: 'Polska', value: 'pl'}
];

export {paymentOptions, deliveryOptions, countryOptions};