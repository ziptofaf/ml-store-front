const MAIN_API_PATH = process.env.NODE_ENV === 'development' ? 'http://localhost:2900/' : 'https://pl.backend.tuger.eu/';

export default MAIN_API_PATH;
