import {store} from '../store/store';
import MAIN_API_PATH from '../constants/urls';

const fetchResourceFromBackEnd = (relative_path, function_if_correct, function_if_fail, params = {}, http_verb = 'GET') => {
    if (relative_path === undefined) {return;}
    let states = store.getState();
    let token = null;
    if ('userSessionState' in states && states.userSessionState.token) {
        token = states.userSessionState.token;
    }
    let headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    };
    if (token !== null) {
        headers['Authorization'] = 'Bearer ' + token;
    }
    if (http_verb === 'GET') {
        fetch(MAIN_API_PATH + relative_path + '?' + transformParamsForGet(params), {
            method: http_verb,
            headers: headers
        })
            .then(response => response.json())
            .then(result => function_if_correct(result))
            .catch(e => function_if_fail(e));
    }
    else {
        fetch(MAIN_API_PATH + relative_path,
            {
                method: http_verb,
                headers: headers,
                body: JSON.stringify(params),
            })
            .then(response => response.json())
            .then(result => function_if_correct(result))
            .catch(e => function_if_fail(e));
    }
};

const transformParamsForGet = (params) => {
    let stringedParams = "";
    const keys = Object.keys(params);

    keys.forEach( (key) => {
            stringedParams += key + '=' + params[key] + '&';
        }
    );
    return stringedParams;
};

store.subscribe(fetchResourceFromBackEnd);

export default fetchResourceFromBackEnd;