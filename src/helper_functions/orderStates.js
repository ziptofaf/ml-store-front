const orderStates = [
    {value: 'new', name: 'W przygotowaniu'},
    {value: 'send', name: 'Wysłano'},
    {value: 'cancelled', name: 'Anulowano'}
];

export {orderStates}