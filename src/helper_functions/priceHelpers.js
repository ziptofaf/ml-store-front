import {deliveryOptions} from "../constants/orderOptions";

function sumProductsInBasket(basket, deliveriesSum = 0) {
    return basket.reduce((acc,item) => acc + item.price * item.quantity, 0) + deliveriesSum;
}

function sumVatInBasket(basket, deliveriesSum = 0) {
    const priceSummed = basket.reduce((acc,item) => acc + item.price * item.quantity, 0) + deliveriesSum;
    const vatless = priceSummed / 1.23;
    return priceSummed - vatless;
}

function sumDeliveriesInBasket(basket, deliveryOption) {
    const deliveryPrice = deliveryOptions.find((item) => item.value === deliveryOption).price;
    const quantities = basket.reduce((acc,item) => acc + item.quantity, 0);
    return quantities * deliveryPrice;
}

function formatPriceToString(number) {
    return number.toFixed(2) + ' zł';
}

function priceWithQuantity(product) {
    return (product.price * product.quantity);
}

export {sumProductsInBasket, sumVatInBasket, sumDeliveriesInBasket, formatPriceToString, priceWithQuantity}