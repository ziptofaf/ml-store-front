function itemsCountToWord(items) {
    if (items.length === 0) {return "przedmiotów";}
    if (items.length === 1) {return "przedmiot";}
    if (items.length < 5) {return "przedmioty";}
    if (items.length >= 5) {return "przedmiotów";}
}

export {itemsCountToWord}