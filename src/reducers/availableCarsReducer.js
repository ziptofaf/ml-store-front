import {CARS_AVAILABLE_CHANGE} from "../constants/actionTypes";

const available_cars = {make: [], model: [], bodyType: [], year: [], selectedCar: undefined};

function availableCarsReducer(state = available_cars, action) {
    switch(action.type) {
        case CARS_AVAILABLE_CHANGE : {
            return availableCarsChange(state, action);
        }
        default :
            return state;
    }
}

function availableCarsChange(state, action) {
    return Object.assign(action.availableCars, {selectedCar: action.selectedCar});
}

export default availableCarsReducer;