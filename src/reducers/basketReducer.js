import {BASKET_ADD, BASKET_EMPTY, BASKET_REMOVE} from "../constants/actionTypes";

const initialState = {
    inBasket: []
};

function basketReducer(state = initialState, action) {
    switch(action.type) {
        case BASKET_ADD : {
            return addToBasket(state, action);
        }
        case BASKET_REMOVE : {
            return removeFromBasket(state, action);
        }
        case BASKET_EMPTY : {
            return emptyBasket();
        }
        default :
            return state;
    }
}

function addToBasket(state, action) {
    console.log(action.product);
    let index = state.inBasket.findIndex((item) => item.id === action.product.id);
    let newQuantity = index !== -1 ? state.inBasket[index].quantity + action.quantity : action.quantity;
    let newProductList = state.inBasket.filter((item) => item.id !== action.product.id);
    let newObj = {...action.product, ...{quantity: newQuantity}};
    newProductList.push(newObj);
    return {inBasket: newProductList};
}

function removeFromBasket(state, action) {
    let newQuantity = action.quantity;
    let newProductList = state.inBasket.filter((item) => item.id !== action.product.id);
    let newObj = {...action.product, ...{quantity: newQuantity}};
    if (newQuantity > 0) {
        newProductList.push(newObj);
    }
    return {inBasket: newProductList};
}

function emptyBasket() {
    return {inBasket: []}
}

export default basketReducer;