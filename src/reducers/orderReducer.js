//snake case as this is the same format that Rails back-end will expect
import {ORDER_CHANGE} from "../constants/actionTypes";

const initialState = {
    email: '',
    billing_name: '',
    billing_surname: '',
    billing_phone: '',
    billing_address: '',
    billing_city: '',
    billing_post_code: '',
    billing_country: 'pl',
    delivery_name: '',
    delivery_surname: '',
    delivery_phone: '',
    delivery_address: '',
    delivery_city: '',
    delivery_post_code: '',
    delivery_country: 'pl',
    tax_number: '',
    comment: '',
    order_items_attributes: [],  //objects with sellable_type (eg. Towbar), quantity, sellable_id
    payment_attributes: {payment_system: 'cash_on_delivery'},
    delivery_option: 'dpd'
};

function orderReducer(state=initialState, action) {
    switch (action.type) {
        case ORDER_CHANGE: {
            return orderChange(state, action)
        }
        default:
            return state;

    }
}

function orderChange(state, action) {
    return {...state, ...action.attribute};
}

export default orderReducer;