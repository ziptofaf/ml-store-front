import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import logger from 'redux-logger';


import userSessionReducer from '../reducers/userSessionReducer';
import availableCarsReducer from '../reducers/availableCarsReducer';
import basketReducer from '../reducers/basketReducer';
import orderReducer from "../reducers/orderReducer";

const rootReducer = combineReducers({
    userSessionState: userSessionReducer,
    availableCarsState: availableCarsReducer,
    basketState: basketReducer,
    orderState: orderReducer
});

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['userSessionState']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, undefined, applyMiddleware(logger));
let persistor = persistStore(store);
export {store, persistor};
